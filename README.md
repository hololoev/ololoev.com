# ololoev.com

## Install

```
sudo docker-compose up -d
```

## Cretae mysql dump 

```
sudo docker exec -i ololoevcom_db_1 mysqldump -uroot -p??? --databases ololoev --skip-comments > /opt/www/ololoev_dump.sql
```


## Import mysql dump

```
sudo docker exec -i ololoevcom_db_1 mysql -uroot -p??? ololoev < /opt/www/ololoev_dump.sql;
```

## How to upgrade

### Add swap 

```
sudo -s
fallocate -l 1G /swapfile
chown root:root /swapfile
chmod 0600 /swapfile
mkswap /swapfile
swapon /swapfile
swapon -s
```
