async function sidebarCommunities() {
  const url = '/api/pub/communities';
  
  try {
    var response = await apiRequest(url);
  } catch {
    return;
  }

  let html = '<ul>';
  for(let item of response) {
    html += `<li><a href="/community/${item.url}">${item.title}</a></li>`;
  }
  
  html += `</ul>`;
  
  let helper = document.getElementById('sidebar_communities');
  helper.innerHTML = html;
}

sidebarCommunities();
