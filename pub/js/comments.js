
const __signLength = 8;

function toString(val) {
  if( typeof(val) === 'undefined' ) return '';
  if( val === null ) return '';
  
  return val.toString();
}

function int32ToBytes(num) {
  arr = new Uint8Array([
    (num & 0xff000000) >> 24,
    (num & 0x00ff0000) >> 16,
    (num & 0x0000ff00) >> 8,
    (num & 0x000000ff)
  ]);
  return arr;
}

function getCharValue(char) {
  let bytes = int32ToBytes(char.charCodeAt(0));
  let result = new Uint8Array([ 0 ]);
  
  for(let index=0; index<bytes.byteLength; index++)
    result[ 0 ] = result[ 0 ] ^ bytes[ index ];

  return result;
}

function sign(srcString, length) {
  let result = new Uint8Array(length);
  let index = 0;
  let randomizer = new Uint8Array([ 0 ]);
  
  for(let char of srcString) {
    let charVal = getCharValue(char);
    result[ index ] = result[ index ] ^ charVal[ 0 ] ^ randomizer[ 0 ];
    randomizer[ 0 ] = randomizer[ 0 ] ^ result[ index ];
    
    index ++;
    if( index >= result.byteLength) index = 0;
  }
  
  let resultString = '';
  for(index=0; index<result.byteLength; index++)
    resultString += result[ index ].toString(16).padStart(2, '0');
  
  return resultString;
}

async function commentFormSubmitClick(event) {
  
  document.getElementById('comment-awaiter').classList.remove('d-none');
  document.getElementById('submit-comment-button').disabled = true;
  
  let commentData = {
    post_id: parseInt(document.getElementById('comment-post-id').value) || null,
    author_name: document.getElementById('comment-user-name').value || null,
    src_body: document.getElementById('comment-editor').value || null
  }
  
  let token = getCookie('access_token') || null;
  let srcString = toString(commentData.post_id) + toString(commentData.author_name) + toString(commentData.src_body) + toString(token);
  
  commentData.signature = sign(srcString, __signLength);

  let result = await apiRequest('/api/comments', 'POST', commentData, 'form-commemt-form-alerts');
  
  document.getElementById('comment-awaiter').classList.add('d-none');
  document.getElementById('submit-comment-button').disabled = false;
  
  document.getElementById('comment-editor').value = '';
  
  // TODO Use xhr to upsate comments
  document.location.reload();
}

function commentEditorTabClick() {
  document.getElementById('comment-md-editor-button').classList.add('active');
  document.getElementById('md-editor-tab').classList.add('active');
  document.getElementById('md-editor-tab').classList.add('show');
  
  document.getElementById('comment-preview-button').classList.remove('active');
  document.getElementById('preview-tab').classList.remove('active');
  document.getElementById('preview-tab').classList.remove('show');
}

function commentPreviewTabClick() {
  document.getElementById('comment-preview-button').classList.add('active');
  document.getElementById('preview-tab').classList.add('active');
  document.getElementById('preview-tab').classList.add('show');
  
  document.getElementById('comment-md-editor-button').classList.remove('active');
  document.getElementById('md-editor-tab').classList.remove('active');
  document.getElementById('md-editor-tab').classList.remove('show');
  
  let converter = new showdown.Converter();
  let prv = document.getElementById('comment-preview');
  let srcText = document.getElementById('comment-editor').value;
  
  prv.innerHTML = replaceHeaders( converter.makeHtml(srcText) );
}

function generateReplayCommentForm(element, id) {
  
  let newDiv = document.createElement('div');
  newDiv.innerHTML = `<div class="text-center"><img src="/img/ajax-loader-big.gif"/></div>`;
  newDiv.classList.add('comment-relay-container');
  newDiv.setAttribute('style', `width: ${element.parentNode.clientWidth}px; top: ${element.parentNode.clientHeight}px;`);
  newDiv.commentReplayParent = 1;
  newDiv.addEventListener('mouseout', commentDynamicViewOut);
  
  return newDiv;
}

function commentDynamicViewOver(element, id) {  
  if( element.replayActive ) return;
  element.replayActive = true;
  
  let replayComment = generateReplayCommentForm(element, id);
  
  element.appendChild(replayComment);
}

function commentDynamicViewOut(event) {
  if( !event.target.commentReplayParent )
    return;
}

function replaceCommentReplaces(match) {
  let commentID = parseInt(match.split('@')[ 1 ]);
  return `
  <a name="comment-${commentID}"></a>
  <span class="comment-replay-link" id="comment-${commentID}" rel="${commentID}" onmouseover="commentDynamicViewOver(this, ${commentID})">${match}</span>`;
}

function assignReplays() {
  let commentList = document.getElementsByClassName('comment-body');
  
  if( commentList.length === 0 ) return;
  
  for(let index=0; index<commentList.length; index++) {
    commentList[ index ].innerHTML = commentList[ index ].innerHTML.replace(/\@[0-9]*/gi, replaceCommentReplaces);
  }
}

function assignCommetFormListeners() {
  document.getElementById('submit-comment-button').addEventListener("click", commentFormSubmitClick, false);
  document.getElementById('comment-md-editor-button').addEventListener("click", commentEditorTabClick, false);
  document.getElementById('comment-preview-button').addEventListener("click", commentPreviewTabClick, false);

  assignReplays();
}

function commentReplayClick(id) {
  const editor = document.getElementById('comment-editor');
  editor.value += `>>${id}\n\n`
}

function assignLiveCommentView(commentsPs) {
  for(let p of commentsPs) {
    // Замена для ссылок на комменты
    p.innerHTML = p.innerHTML.replaceAll(/(\&gt\;)(\&gt\;)[0-9]+/g, function(src) {
      let id = src.replace(/(\&gt\;)(\&gt\;)/, '');
      return `<div class="commemtLiveView" onmouseout="commentLiveViewFade(this)" onmouseover="commentLiveView(this, ${id})"><a href='#comment-${id}'>&gt;&gt;${id}</a><div class="subcomment-container"></div></div>`;
    });
    
    // Замена для цитирований
    p.innerHTML = p.innerHTML.replaceAll(/(\&gt\;)\s(.)+$/g, function(src) {
      return `<span class="citate">${src}</span>`;
    });
  }
}

function commentLiveViewFade(src) {
  if( !src.mouseHovered ) return;
  src.mouseHovered = false;
  src.fadeTimeout = setTimeout(function() {
    if( src.mouseHovered ) return;
    
    const container = src.getElementsByClassName('subcomment-container')[ 0 ];
    container.style.display = 'none';
  }, 1000);
}

function makeLiveViewForm(comment) {
  let containerWidth = document.getElementsByClassName('comment')[ 0 ].clientWidth;
  
  let html = `<div style="width: ${containerWidth}px;">
<div class="comment" id="comment-${comment.id}">
<a name="comment-${comment.id}"></a>
  <div class="comment-header">
    <div class="row">
      <div class="col-sm-7 text-left">
        <span class="">ID: </span><a href="#comment-${comment.id}" onclick="commentReplayClick(${comment.id})">#${comment.id}</a>
        &nbsp
        <span class="text-muted">Создан: </span><span class="post-dates text-muted" ref="${comment.created_at}">${dateTimeFormat(new Date(comment.created_at))}</span>
      </div>
      <div class="col-sm-5 text-right">
        <span class="">Автор: </span><span class="">${comment.author_name}</span>
      </div>
    </div>
  </div>

  <div class="comment-body">
  ${comment.body}
  </div>
</div>
</div>`;
  return html;
}

async function commentLiveView(src, id) {
  if( src.mouseHovered ) return;
  src.mouseHovered = true;
  
  const container = src.getElementsByClassName('subcomment-container')[ 0 ];
  container.style.display = 'block';
  
  if( src.subComment ) {
    container.innerHTML = src.subComment;
    assignLiveCommentView(container.querySelectorAll('.comment-body p'));
    return
  }
  
  container.innerHTML = `<div><img src="/img/ajax-loader_flat-small.gif"/></div>`;
  
  let comments = await apiRequest(`/api/comments/${id}`, 'GET', null, container);
  if( comments ) {
    src.subComment = makeLiveViewForm(comments[ 0 ]);
    container.innerHTML = src.subComment;
    assignLiveCommentView(container.querySelectorAll('.comment-body p'));
  }
}

assignCommetFormListeners();
assignLiveCommentView(document.querySelectorAll('.comment-body p'));
