
function editorTabClick() {
  document.getElementById('md-editor-button').classList.add('active');
  document.getElementById('md-editor-tab').classList.add('active');
  document.getElementById('md-editor-tab').classList.add('show');
  
  document.getElementById('preview-button').classList.remove('active');
  document.getElementById('preview-tab').classList.remove('active');
  document.getElementById('preview-tab').classList.remove('show');
}

function previewTabClick() {
  document.getElementById('preview-button').classList.add('active');
  document.getElementById('preview-tab').classList.add('active');
  document.getElementById('preview-tab').classList.add('show');
  
  document.getElementById('md-editor-button').classList.remove('active');
  document.getElementById('md-editor-tab').classList.remove('active');
  document.getElementById('md-editor-tab').classList.remove('show');
  
  let converter = new showdown.Converter();
  let prv = document.getElementById('post-preview');
  let srcText = document.getElementById('post-editor').value;
  
  prv.innerHTML = replaceHeaders( converter.makeHtml(srcText) );
}

function reDrawCategorySelected() {
  let html = '';
  
  if( selectedCategories.length === 0 ) {
    html = 'Пусто';
    document.getElementById('post-category-selected').innerHTML = html;
    return;
  }
  
  for(let item of selectedCategories)
    if( item ) { // can be null !!
      html += `<div class="selected-list-item">
        <label>${item.title}</label>
        <span class="selected-list-delete-item btn btn-sm btn-danger" onclick="removeListItem(${item.id})">X</span>
      </div>`;
    }
  
  document.getElementById('post-category-selected').innerHTML = html;
}

function removeListItem(id) {
  let select = document.getElementById('post-category');
  
  for(let index=0; index<selectedCategories.length; index++)
    if( selectedCategories[ index ] && selectedCategories[ index ].id === id ) {
      delete( selectedCategories[ index ] );
      
      for(let sIndex=0; sIndex<select.options.length; sIndex++)
        if( parseInt(select.options[ sIndex ].value) === id )
          select.options[ sIndex ].disabled = false;
    }
  
  reDrawCategorySelected();
}

function categorySelect(select) {
  selectedCategories.push({ id: parseInt(select.selectedOptions[ 0 ].value), title: select.selectedOptions[ 0 ].innerHTML });
  select.selectedOptions[ 0 ].disabled = true;
  reDrawCategorySelected();
}

async function saveClick() {
  if( saveInProgress ) return;

  saveInProgress = true;
  document.getElementById('save-button').classList.add('disabled');
  
  let categories = [];
  for(let cat of selectedCategories)
    if( cat ) categories.push(cat.id);

  let post = {
    status: parseInt(document.getElementById('post-status').value),
    title: document.getElementById('post-title').value,
    community_id: parseInt(document.getElementById('post-community').value) || null,
    image_url: document.getElementById('post-img-preview').value,
    excerpt: document.getElementById('post-excerpt').value,
    src_body: document.getElementById('post-editor').value,
    categories: categories
  };
  
  if( postId ) {
    await apiRequest(`/api/posts/${postId}`, 'PUT', post);
  } else {
    let response = await apiRequest('/api/posts', 'POST', post);
    location.href = `/profile/post/${response[ 0 ].id}`;
    return;
  }
  
  document.getElementById('save-button').classList.remove('disabled');
  
  saveInProgress = false;
}

function assignValues(pageData) {
  document.getElementById('post-img-preview').value = pageData.image_url;
  document.getElementById('post-excerpt').value = pageData.excerpt;
  document.getElementById('post-status').value = pageData.status;
  document.getElementById('post-title').value = pageData.title;
  document.getElementById('post-editor').value = pageData.src_body;
  document.getElementById('post-community').value = pageData.community_id;
  //reDrawCategorySelected();
}

async function init() {
  let communities = await apiRequest('/api/communities');
  if( !communities ) return;
  assignSelect(communities, 'post-community', 'id', 'title');

// TODO 
//   let categories = await apiRequest('/api/categories');
//   if( !categories ) return;
//   assignSelect(categories, 'post-category', 'id', 'title');
  
  // Init user files feature
  userFiles = new UserFiles(
    document.getElementById('upload_files'),
    document.getElementById('upload_q'),
    document.getElementById('attached_files'),
    document.getElementById('post-editor'),
    document.getElementById('post-img-preview'),
    'post', postId || ''
  );
      
  if( postId ) {
    let postList = await apiRequest(`/api/posts/${postId}`);
    if( postList ) {
      postData = postList[ 0 ];
      
      let select = document.getElementById('post-category');
      selectedCategories = [];
      
      for(let cat of postData.category_relations) {
        selectedCategories.push({
          id: cat.category_id,
          title: cat.category.title
        });
        
        for(let sIndex=0; sIndex<select.options.length; sIndex++)
          if( parseInt(select.options[ sIndex ].value) === cat.category_id )
            select.options[ sIndex ].disabled = true;
      }
      
      assignValues(postList[ 0 ]);
    }
  } else {
    // TODO
  }
}

init();
