 
function dateTimeFormat(date) {
  let day = date.getDate();
  if( day < 10 ) day = `0${day}`;
  
  let month = date.getMonth() + 1;
  if( month < 10 ) month = `0${month}`;
  
  let year = date.getFullYear();
  
  let hours = date.getHours();
  if( hours < 10 ) hours = `0${hours}`;
  
  let minutes = date.getMinutes();
  if( minutes < 10 ) minutes = `0${minutes}`;
  
  return `${day}.${month}.${year} ${hours}:${minutes}`;
}

function setPreviewDates(className) {
  let elements = document.getElementsByClassName(className);
  for(let index=0; index<elements.length; index++) {
    let jsDate = new Date(elements[ index ].attributes.ref.nodeValue);
    elements[ index ].innerHTML = dateTimeFormat(jsDate);
  }
}
