class UserFiles {
  __uploadDoneStatus = 3;
  __refreshInterval = 1000;
  __usrImageWebFolder = '/usrimg';
  __maxFileSize = 1024 * 1024 * 16;
  __localStorageKey = 'images_cache';
  __imgLiveTime= 1000 * 60 * 60 * 24 * 7;
  __emptyPageimgLiveTime = 1000 * 60 * 30;
  __imageStatuses = [ '', 'В очереди на обработку', 'Обрабатывается', 'Загружено' ];
  
  fileElement;
  iconElement;
  load_q = [];
  editorEliment;
  upload_qElement;
  storageKey = '';
  q_status = false;
  savedFilesDB = {};
  storagePrefix = '';
  refreshLocked = false;
  attached_filesElement;
  refreshInterval = null;
  
  constructor(fileElement, upload_qElement, attached_filesElement, editorEliment, iconElement, storagePrefix='', storageKey='') {
    this.fileElement = fileElement;
    this.iconElement = iconElement;
    this.editorEliment = editorEliment;
    this.upload_qElement = upload_qElement;
    this.attached_filesElement = attached_filesElement;
    
    this.fileElement.addEventListener('change', (event) => this.fileElementChanged(event));
    this.refreshInterval = setInterval(() => this.refreshStatuses(), this.__refreshInterval);
    
    this.storagePrefix = storagePrefix;
    this.storageKey = storageKey;
    
    if( !localStorage[ this.__localStorageKey ] ) {
      let __store = {};
      __store[ `${this.storagePrefix + this.storageKey}` ] = {};
      
      localStorage[ this.__localStorageKey ] = JSON.stringify(__store);
    }
    
    this.restoreStorage();
    this.refreshStatuses();
  }
  
  restoreStorage() {
    let __store = JSON.parse(localStorage[ this.__localStorageKey ]);
    
    for(let prKey in __store)
      for(let hash in __store[ prKey ]) {
        let timePassed = new Date() - __store[ prKey ][ hash ].timestamp;
        
        if( __store[ prKey ][ hash ].key ) {
          if( timePassed > this.__imgLiveTime )
            delete __store[ prKey ][ hash ];
          
          continue;
        }
        
          if( timePassed > this.__emptyPageimgLiveTime )
            delete __store[ prKey ][ hash ];
      }
      
    localStorage[ this.__localStorageKey ] = JSON.stringify(__store);
    
    for(let hash in __store[ this.storagePrefix + this.storageKey ])
      if( this.savedFilesDB[ hash ] = __store[ this.storagePrefix + this.storageKey ][ hash ] )
        this.savedFilesDB[ hash ] = __store[ this.storagePrefix + this.storageKey ][ hash ].data;
    
    for(let hash in __store[ this.storagePrefix ]) 
      if( this.savedFilesDB[ hash ] = __store[ this.storagePrefix ][ hash ] )
        this.savedFilesDB[ hash ] = __store[ this.storagePrefix ][ hash ].data;
  }
  
  saveToStorage(hash, data) {
    let __store = JSON.parse(localStorage[ this.__localStorageKey ]);
    
    if( !__store[ this.storagePrefix + this.storageKey ] )
      __store[ this.storagePrefix + this.storageKey ] = {};
    
    __store[ this.storagePrefix + this.storageKey ][ hash ] = { data, timestamp: new Date(), key: this.storageKey };
    
    localStorage[ this.__localStorageKey ] = JSON.stringify(__store);
  }
  
  updateToStorage(hash, data) {
    let __store = JSON.parse(localStorage[ this.__localStorageKey ]);
    
    if( !__store[ this.storagePrefix + this.storageKey ] )
      __store[ this.storagePrefix + this.storageKey ] = {};
    
    if( !__store[ this.storagePrefix + this.storageKey ][ hash ] )
      __store[ this.storagePrefix + this.storageKey ][ hash ] = { data, timestamp: new Date(), key: this.storageKey };
    else
      __store[ this.storagePrefix + this.storageKey ][ hash ].data = data;
    
    localStorage[ this.__localStorageKey ] = JSON.stringify(__store);
  }
  
  deleteFromStorage(hash) {
    let __store = JSON.parse(localStorage[ this.__localStorageKey ]);
    
    if( !__store[ this.storagePrefix + this.storageKey ] )
      return;
    
    if( __store[ this.storagePrefix + this.storageKey ] )
      if( __store[ this.storagePrefix + this.storageKey ][ hash ] )
        delete __store[ this.storagePrefix + this.storageKey ][ hash ];
    
    if( __store[ this.storagePrefix ] )
      if( __store[ this.storagePrefix ][ hash ] )
        delete __store[ this.storagePrefix ][ hash ];
    
    localStorage[ this.__localStorageKey ] = JSON.stringify(__store);
  }
  
  async refreshStatuses() {
    if( this.refreshLocked ) return;
    
    let hasList = [];
    for(let hash in this.savedFilesDB)
      if( this.savedFilesDB[ hash ].identifier.status !== this.__uploadDoneStatus )
        hasList.push(hash);
    
    if( hasList.length === 0 ) return this.redrawSelectImageElement();
    
    this.refreshLocked = true;
    
    let options = {
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST',
      body: JSON.stringify(hasList)
    };
    
    const request = new Request(`/api/file/status`, options);
    let response = await fetch(request);
    let result = await response.json();
    
    if( !response.ok ) {
      alert(response.statusText + ' / ' + result.meta.error.description);
      console.log(response.statusText, result.meta.error.description);
      this.refreshLocked = false;
      return;
    }
    
    for(let file of result.data) {
      if( this.savedFilesDB[ file.hash ] )
        this.savedFilesDB[ file.hash ].identifier = file;
      
      this.updateToStorage(file.hash, {
        identifier: file,
        imageData: null
      })
    }
    
    this.refreshLocked = false;
    this.redrawSelectImageElement();
  }
  
  fileElementChanged(event) {
    if( this.fileElement.files.length === 0 ) return;
    
    for(let index=0; index<this.fileElement.files.length; index++) {
      let file = this.fileElement.files[ index ];
      
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.load_q.push({
          file: file,
          data: reader.result
        });
        this.qInit();
      }
      
      reader.onerror = () => {
        console.error(reader.error);
      };
    }
  }
  
  async redrawQElement(currentItem=null) {
    let result = '';
    
    if( currentItem )
      result += `<div class="upload-file-q-item">${currentItem.file.name}<div class="upload-file-q-item-progress"></div></div>`;
    
    for(let index=0; index<this.load_q.length; index++) {
      let item = this.load_q[ index ];
      let htmlItem = `<div class="upload-file-q-item">${item.file.name}</div>`;
      result += htmlItem;
    }
    
    this.upload_qElement.innerHTML = result;
  }
  
  async uploadFile(item) {
    let options = {
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST',
      body: JSON.stringify({
        type: item.file.type,
        name: item.file.name,
        data: item.data,
        size: item.file.size
      })
    }
  
    const request = new Request(`/api/file`, options);
    let response = await fetch(request);
    let result = await response.json();
    
    if( !response.ok ) {
      alert(response.statusText + ' / ' + result.meta.error.description);
      console.log(response.statusText, result.meta.error.description);
      return;
    }
    
    return result.data[ 0 ];
  }
  
  async qInit() {
    if( this.q_status ) return;
    
    if( this.load_q.length === 0 ) {
      this.q_status = false;
      return;
    }
    
    this.q_status = true;
    
    while( this.load_q.length ) {
      let item = this.load_q.pop();
      
      if( item.file.size > this.__maxFileSize ) continue;
      
      await this.redrawQElement(item);
      
      let fileIdentifier = await this.uploadFile(item);
      
      this.savedFilesDB[ fileIdentifier.hash ] = {
        identifier: fileIdentifier,
        imageData: item.data,
        created_at: new Date()
      };

      this.saveToStorage(fileIdentifier.hash, {
        identifier: fileIdentifier,
        imageData: null,
      });
    }
    
    this.q_status = false;
    await this.redrawQElement();
    await this.redrawSelectImageElement();
  }
  
  imageClick(event, hash) {
    if( !this.savedFilesDB[ hash ] ) return; // error here !
    
    let info = this.savedFilesDB[ hash ].identifier;
    
    let image = `[![${info.orig_name}](${this.__usrImageWebFolder}/${info.path}/preview.${info.dst_extension})](${this.__usrImageWebFolder}/${info.path}/fullsize.${info.dst_extension})\n`;
    
    this.editorEliment.setRangeText(
      image,
      this.editorEliment.selectionStart,
      this.editorEliment.selectionEnd,
      'end'
    );
  }
  
  imageDeleteClick(event, hash) {
    event.stopPropagation();
    delete this.savedFilesDB[ hash ];
    this.deleteFromStorage(hash);
    document.getElementById(hash).remove();
  }
  
  imageUseIconClick(event, hash) {
    event.stopPropagation();
    let info = this.savedFilesDB[ hash ].identifier;
    this.iconElement.value = `${this.__usrImageWebFolder}/${info.path}/preview.${info.dst_extension}`;
  }
  
  async redrawSelectImageElement() {
    for(let hash in this.savedFilesDB) {
      if( !this.savedFilesDB[ hash ] ) continue;
      
      let already = document.getElementById(hash);
      if( already ) {
        if( this.savedFilesDB[ hash ].identifier.status === this.__uploadDoneStatus ) {
          let preloader = already.getElementsByClassName('image-select-image-progress');
          
          if( preloader.length > 0 )
            preloader[ 0 ].remove();
        }
        
        continue;
      }
      
      let imageSelectBack = document.createElement('div');
      imageSelectBack.classList.add('image-select-back');
      imageSelectBack.id = hash;
      imageSelectBack.onclick = (event) => this.imageClick(event, hash);
      
      let imageSelecImage = document.createElement('img');
      imageSelecImage.classList.add('image-select-image');
      
      if( this.savedFilesDB[ hash ].imageData )
        imageSelecImage.src = this.savedFilesDB[ hash ].imageData;
      else
        imageSelecImage.src = `${this.__usrImageWebFolder}/${this.savedFilesDB[ hash ].identifier.path}/preview.${this.savedFilesDB[ hash ].identifier.dst_extension}`;
      
      if( this.savedFilesDB[ hash ].identifier.status !== this.__uploadDoneStatus ) {
        let imageSelecProgress = document.createElement('div');
        imageSelecProgress.classList.add('image-select-image-progress');
        imageSelectBack.appendChild(imageSelecProgress);
      }
      
      imageSelectBack.appendChild(imageSelecImage);
      
      let imageSelecInfo = document.createElement('div');
      imageSelecInfo.classList.add('image-select-image-info');
      imageSelecInfo.classList.add('small');
      
      let html = `<ul>`;
      html += `<li><label>Имя:</label> ${this.savedFilesDB[ hash ].identifier.orig_name}</li>`;
      html += `<li><label>Размер:</label> ${this.savedFilesDB[ hash ].identifier.orig_size}</li>`;
      html += `<li><label>Статус:</label> ${this.__imageStatuses[ this.savedFilesDB[ hash ].identifier.status ]}</li>`;
      html += `<li><label>Дата:</label> ${this.savedFilesDB[ hash ].identifier.created_at}</li>`;
      html += `</ul>`;
      
      imageSelecInfo.innerHTML = html;
      imageSelectBack.appendChild(imageSelecInfo);
      
      let imageDeleteButton = document.createElement('div');
      imageDeleteButton.classList.add('image-select-image-delete');
      imageDeleteButton.onclick = (event) => this.imageDeleteClick(event, hash);
      imageDeleteButton.innerHTML = 'X';
      imageDeleteButton.title = 'Удалить';
      imageSelectBack.appendChild(imageDeleteButton);
      
      let imageIconButton = document.createElement('div');
      imageIconButton.classList.add('image-select-image-icon');
      imageIconButton.onclick = (event) => this.imageUseIconClick(event, hash);
      imageIconButton.innerHTML = 'ico';
      imageIconButton.title = 'Использовать как иконку';
      imageSelectBack.appendChild(imageIconButton);
      
      this.attached_filesElement.appendChild(imageSelectBack);
    }
  }
}
