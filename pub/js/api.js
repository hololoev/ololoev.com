
function showAlert(text, form, type='warning') {
  if( !form ) return;
  
  let helper;
  
  if( typeof(form) === 'object' )
    helper = form;
  else
    helper = document.getElementById(form);

  if( !helper ) return;
  
  helper.innerHTML = `<div class="alert alert-${type}" role="alert">${text}</div>`;
}

function replaceHeaders(srcText) {
  srcText = srcText.replace(/<h4/gi, '<h5');
  srcText = srcText.replace(/<h3/gi, '<h4');
  srcText = srcText.replace(/<h2/gi, '<h3');
  srcText = srcText.replace(/<h1/gi, '<h2');
  
  srcText = srcText.replace(/<\/h4/gi, '</h5');
  srcText = srcText.replace(/<\/h3/gi, '</h4');
  srcText = srcText.replace(/<\/h2/gi, '</h3');
  srcText = srcText.replace(/<\/h1/gi, '</h2');
  
  return srcText;
}

function assignSelect(list, selectID, key_name, html_name) {
  let select = document.getElementById(selectID);
  
  if( !select ) throw new Error('Element not found');
  
  let html = '';
  for(let item of list) {
    html += `<option value="${item[ key_name ]}">${item[ html_name ]}</option>`;
  }
  
  select.innerHTML += html;
}

async function apiRequest(url, method='GET', body=null, failForm='form-alerts') {
  let options = {
    headers: {
      'Content-Type': 'application/json'
    },
    method: method
  }
  
  if( body ) options.body = JSON.stringify(body);
  const request = new Request(url, options);
  
  try {
    let response = await fetch(request);

    if( response.status !== 200 ) {
      showAlert(response.statusText, failForm);
      return;
    }
    
    let result = await response.json();
    
    if( !response.ok ) {
      showAlert(response.statusText + '<br/>' + result.meta.error.description, failForm);
      return;
    }
  
    return result.data;
  } catch(err) {
    showAlert(err.toString(), failForm);
    return;
  }
}
