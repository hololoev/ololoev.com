
function setCookie(name, value, expires_at=null) {
  let expires = '';
  if (expires_at) expires = '; expires=' + expires_at.toUTCString();
  document.cookie = name + '=' + (value || '')  + expires + '; path=/; SameSite=Strict';
}

function getCookie(name) {
  let nameEQ = name + '=';
  let ca = document.cookie.split(';');
  for(let i=0;i < ca.length;i++) {
    let c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}

function deleteCookie(name) {
  document.cookie = name+'=; Max-Age=-99999999;';
}
