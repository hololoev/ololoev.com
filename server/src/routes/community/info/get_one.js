
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const commonSchemes = require('../../../libs/schemes/common');
const __communities = require('../../../constants/communities');

function setOptionValue(val, key) {
  if ( !__communities[ key ] )
    return 'Error';

  if ( __communities[ key ][ val ] )
    return __communities[ key ][ val ].name;

  return 'Error';
}

function setTime(val) {
  return `${parseInt(val/1000, 10)} сек`;
}

const __options = {
  type: { name: 'Тип сообщества', set: function(val) {
    return setOptionValue(val, 'communityTypesById');
  } },
  public_type: { name: 'Публичность сообщества', set: function(val) {
    return setOptionValue(val, 'publicTypesById');
  } },
  entry: { name: 'Вступление в сообщество', set: function(val) {
    return setOptionValue(val, 'entryTypesById');
  } },
  anon_comments_allow: { name: 'Анонимные комментарии', set: function(val) {
    return setOptionValue(val, 'statusesById');
  } },
  anon_comments_timeout: { name: 'Таймаут на анонимные комментарии', set: setTime },
  non_community_comment_allow: { name: 'Комментарии от НЕ анонимных НЕ членов сообщества', set: function(val) {
    return setOptionValue(val, 'statusesById');
  } },
  non_community_comment_timeout: { name: 'Таймаут на комментарии от НЕ анонимных НЕ членов сообщества', set: setTime },
  community_comment_timeout: { name: 'Таймаут на комментарии от членов сообщества', set: setTime },
};

async function response(request, h) {
  let authorized = false;
  let user = false;

  try {
    const { artifacts } = await request.server.auth.test('token', request);
    authorized = true;
    user = artifacts.user;
  } catch {}

  const users = request.getModel(request.server.config.db.database, 'users');
  const communities = request.getModel(request.server.config.db.database, 'communities');
  const community_relations = request.getModel(request.server.config.db.database, 'community_relations');

  let query = {
    where: {
      url: request.params.url
    }
  };

  query.include = [
    {
      model: users,
      as: 'author',
      attributes: [ 'id', 'name', 'role', 'status' ]
    }
  ];

  let result = await communities.findOne(query);

  if ( !result )
    throw Boom.notFound('Сообщество не найдено');

  result = result.get({ plain: true });

  let isInCommunity = false;

  if ( user )
    isInCommunity = await community_relations.findOne({
      where: {
        community_id: result.id,
        user_id: user.id
      }
    });

  let options = [];
  for (let key in __options)
    options.push({ name: __options[ key ].name, value: __options[ key ].set(result[ key ]) });

  let page = {
    authorized: authorized,
    user: user,
    isInCommunity: isInCommunity,
    page_url: `/community/${request.params.url}/info`,
    page_title: result.title,
    page_description: result.description || result.excerpt || '',
    page_image: result.image_url || '/img/default-200x150.jpg',
    page_create_date: result.created_at,
    page_body: result,
    page_options: options,
    page_meta: {
      query: request.query || null,
      totalCount: null,
      totalPageCount: null
    }
  };

  return h.view('community-info', page);
}

module.exports = {
  method: 'GET',
  path: '/community/{url}/info',
  options: {
    handler: response,
    description: 'Returns community page',
    tags: [ 'api', 'content', 'noTest' ],
    auth: false,
    validate: {
      headers: commonSchemes.authHeaders,
      params: Joi.object({
        url: Joi.string().max(255).required().example('olololo')
      })
    },
    //response: { schema: commonSchemes.responseWrapper() }
  }
};
