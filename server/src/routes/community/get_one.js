
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const commonSchemes = require('../../libs/schemes/common');

const __defaultPageLimit = 10;
const __defaultPageNum = 1;

async function response(request, h) {
  let authorized = false;
  let user = false;

  try {
    const { artifacts } = await request.server.auth.test('token', request);
    authorized = true;
    user = artifacts.user;
  } catch {}

  const sequelize = request.getDb().sequelize;
  const users = request.getModel(request.server.config.db.database, 'users');
  const posts = request.getModel(request.server.config.db.database, 'posts');
  const communities = request.getModel(request.server.config.db.database, 'communities');
  const community_relations = request.getModel(request.server.config.db.database, 'community_relations');

  let query = {
    where: {
      url: request.params.url
    }
  };

  query.include = [
    {
      model: users,
      as: 'author',
      attributes: [ 'id', 'name', 'role', 'status' ]
    }
  ];

  let result = await communities.findOne(query);

  if ( !result )
    throw Boom.notFound('Сообщество не найдено');

  result = result.get({ plain: true });

  let isInCommunity = false;

  if ( user )
    isInCommunity = await community_relations.findOne({
      where: {
        community_id: result.id,
        user_id: user.id
      }
    });

  // Then get posts for this community
  let postQuery = {
    where: {
      status: posts.statuses.ENABLED.id,
      community_id: result.id
    },
    limit: request.query.limit,
    offset: request.query.limit * (request.query.page-1),
    order: [ [ 'published_at', 'DESC' ] ]
  };

  postQuery.attributes = [ 'id', 'author_id', 'community_id', 'status', 'admin_status', 'admin_comment',
    'url', 'image_url', 'description' ,'title', 'excerpt', 'published_at', 'created_at', 'updated_at',
    [ sequelize.literal(`(SELECT COUNT(*) FROM comments cmm WHERE cmm.post_id=posts.id AND cmm.admin_status>0)`), 'comments_count' ]
  ];

  postQuery.include = [];

  let postResult = await posts.findAll(postQuery);
  postResult = postResult.map(el => el.get({ plain: true }));
  let totalCount = await posts.count({ where: { community_id: result.id } });

  let totalPageCount = parseInt(totalCount / request.query.limit);
  if ( (totalCount % request.query.limit) > 0 ) totalPageCount += 1;

  let page = {
    authorized: authorized,
    user: user,
    isInCommunity: isInCommunity,
    page_url: `/community/${request.params.url}`,
    page_title: result.title,
    page_description: result.description || result.excerpt || '',
    page_image: result.image_url || '/img/default-200x150.jpg',
    page_create_date: result.created_at,
    page_body: result,
    posts_list: postResult,
    page_meta: {
      query: request.query || null,
      totalCount: totalCount,
      totalPageCount: totalPageCount
    }
  };

  if ( (request.query.page - 1) >= 1 ) page.page_meta.prev_page = request.query.page - 1;
  if ( (request.query.page + 1) <= totalPageCount ) page.page_meta.next_page = request.query.page + 1;

  return h.view('community-view', page);
}

module.exports = {
  method: 'GET',
  path: '/community/{url}',
  options: {
    handler: response,
    description: 'Returns community page',
    tags: [ 'api', 'content', 'noTest' ],
    auth: false,
    validate: {
      headers: commonSchemes.authHeaders,
      params: Joi.object({
        url: Joi.string().max(255).required().example('olololo')
      }),
      query: Joi.object({
        page: Joi.number().integer().default(__defaultPageNum).description('Page number').example(__defaultPageNum),
        limit: Joi.number().integer().default(__defaultPageLimit).description('Post count on the page').example(__defaultPageLimit)
      })
    },
    //response: { schema: commonSchemes.responseWrapper() }
  }
};
