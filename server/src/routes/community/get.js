
const queryHelper = require('../../libs/queryHelper');
const commonSchemes = require('../../libs/schemes/common');

async function response(request, h) {
  let authorized = false;
  let user = false;

  try {
    const { artifacts } = await request.server.auth.test('token', request);
    authorized = true;
    user = artifacts.user;
  } catch {}

  const communities = request.getModel(request.server.config.db.database, 'communities');

  let query = { limit: request.query.limit, offset: request.query.limit * (request.query.page-1) };

  query.attributes = [ 'id', 'author_id', 'status', 'admin_status', 'admin_comment',
    'url', 'image_url', 'description' ,'title', 'excerpt', 'created_at', 'updated_at' ];

  query.include = [];

  let result = await communities.findAll(query);
  result = result.map(el => el.get({ plain: true }));
  let totalCount = await communities.count(queryHelper.parseQueryParams(request.query));

  let totalPageCount = parseInt(totalCount / request.query.limit);
  if ( (totalCount % request.query.limit) > 0 ) totalPageCount += 1;

  let page = {
    authorized: authorized,
    user: user,
    page_url: '/community',
    page_title: 'Сообщества',
    page_description: 'Список сообществ на сайте',
    page_image: '',
    page_create_date: new Date(),
    page_body: result,
    page_meta: {
      query: request.query,
      totalCount: totalCount,
      totalPageCount: totalPageCount
    }
  };

  if ( (request.query.page - 1) >= 1 ) page.page_meta.prev_page = request.query.page - 1;
  if ( (request.query.page + 1) <= totalPageCount ) page.page_meta.next_page = request.query.page + 1;

  return h.view('community-list', page);
}

module.exports = {
  method: 'GET',
  path: '/community',
  options: {
    handler: response,
    description: 'Returns communities list',
    tags: [ 'api', 'content', 'noTest' ],
    auth: false,
    validate: {
      headers: commonSchemes.authHeaders,
      query: commonSchemes.defaultPageQueryScheme
    },
    //response: { schema: commonSchemes.responseWrapper() }
  }
};
