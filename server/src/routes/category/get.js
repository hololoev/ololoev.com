
const Joi = require('@hapi/joi');

const queryHelper = require('../../libs/queryHelper');
const commonSchemes = require('../../libs/schemes/common');

const __defaultPageLimit = 3;
const __defaultPageNum = 1;

async function response(request, h) {
  let authorized = false;
  let user = false;

  try {
    const { artifacts } = await request.server.auth.test('token', request);
    authorized = true;
    user = artifacts.user;
  } catch {}

  const categories = request.getModel(request.server.config.db.database, 'categories');

  let query = { limit: request.query.limit, offset: request.query.limit * (request.query.page-1) };

  query.attributes = [ 'id', 'author_id', 'status',
    'url', 'image_url', 'description' ,'title', 'excerpt', 'created_at', 'updated_at' ];

  query.include = [];

  let result = await categories.findAll(query);
  result = result.map(el => el.get({ plain: true }));
  let totalCount = await categories.count(queryHelper.parseQueryParams(request.query));

  let totalPageCount = parseInt(totalCount / request.query.limit);
  if ( (totalCount % request.query.limit) > 0 ) totalPageCount += 1;

  let page = {
    authorized: authorized,
    user: user,
    page_url: '/category',
    page_title: 'Категори',
    page_description: 'Список категорий на сайте',
    page_image: '',
    page_create_date: new Date(),
    page_body: result,
    page_meta: {
      query: request.query,
      totalCount: totalCount,
      totalPageCount: totalPageCount
    }
  };

  if ( (request.query.page - 1) >= 1 ) page.page_meta.prev_page = request.query.page - 1;
  if ( (request.query.page + 1) <= totalPageCount ) page.page_meta.next_page = request.query.page + 1;

  return h.view('category-list', page);
}

module.exports = {
  method: 'GET',
  path: '/category',
  options: {
    handler: response,
    description: 'Returns category list',
    tags: [ 'api', 'content', 'noTest' ],
    auth: false,
    validate: {
      headers: commonSchemes.authHeaders,
      query: Joi.object({
        page: Joi.number().integer().default(__defaultPageNum).description('Page number').example(__defaultPageNum),
        limit: Joi.number().integer().default(__defaultPageLimit).description('Post count on the page').example(__defaultPageLimit)
      })
    },
    //response: { schema: commonSchemes.responseWrapper() }
  }
};
