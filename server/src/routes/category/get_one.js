
const Joi = require('@hapi/joi');

const commonSchemes = require('../../libs/schemes/common');

async function response(request, h) {
  let authorized = false;
  let user = false;

  try {
    const { artifacts } = await request.server.auth.test('token', request);
    authorized = true;
    user = artifacts.user;
  } catch {}

  const categories = request.getModel(request.server.config.db.database, 'categories');

  let query = {
    where: {
      url: request.params.url
    }
  };

  //query.attributes = [ 'id', 'author_id', 'community_id', 'status', 'admin_status', 'admin_comment',
  //  'url', 'image_url', 'description' ,'title', 'excerpt', 'created_at', 'updated_at' ];

  let result = await categories.findOne(query);
  result = result.get({ plain: true });

  let page = {
    authorized: authorized,
    user: user,
    page_url: `/category/${request.params.url}`,
    page_title: result.title,
    page_description: result.description || result.excerpt || '',
    page_image: result.image_url || '/img/default-200x150.jpg',
    page_create_date: result.created_at,
    page_body: result,
    page_meta: {
      query: request.query || null,
      totalCount: null,
      totalPageCount: null
    }
  };

  return h.view('category-view', page);
}

module.exports = {
  method: 'GET',
  path: '/category/{url}',
  options: {
    handler: response,
    description: 'Returns category page',
    tags: [ 'api', 'content', 'noTest' ],
    auth: false,
    validate: {
      headers: commonSchemes.authHeaders,
      params: Joi.object({
        url: Joi.string().max(255).required().example('olololo')
      })
    },
    //response: { schema: commonSchemes.responseWrapper() }
  }
};
