
const Joi = require('@hapi/joi');

const userConst = require('../../../constants/users');

async function response(request, h) {

  let page = {
    authorized: true,
    isAdmin: request.auth.artifacts.user.role <= userConst.roles.MODERATOR.id,
    user: request.auth.artifacts.user,
    page_url: `/profile/comment/${request.params.id}`,
    page_title: 'Редактор коммента',
    page_description: 'Редактор коммента',
    page_image: '',
    page_create_date: new Date(),
    page_body: {
      post: {}
    },
    page_meta: {
      commentId: request.params.id
    }
  };

  return h.view('profile-comment-edit', page);
}


module.exports = {
  method: 'GET',
  path: '/profile/comment/{id}',
  options: {
    handler: response,
    description: 'Edir comment',
    tags: [ 'api', 'content', 'noTest' ],
    auth: 'token',
    validate: {
      params: Joi.object({
        id: Joi.number().integer().min(0).description('Comments ID').example(1)
      })
    },
    //response: { schema: commonSchemes.responseWrapper() }
  }
};
