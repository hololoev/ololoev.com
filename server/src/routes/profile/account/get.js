
const userConst = require('../../../constants/users');

async function response(request, h) {

  let page = {
    authorized: true,
    isAdmin: request.auth.artifacts.user.role <= userConst.roles.MODERATOR.id,
    user: request.auth.artifacts.user,
    page_url: '/profile/account',
    page_title: 'Настройки аккаунта',
    page_description: 'Настройки аккаунта',
    page_image: '',
    page_create_date: new Date(),
  };

  return h.view('profile-account', page, { layout: 'profile' });
}


module.exports = {
  method: 'GET',
  path: '/profile/account',
  options: {
    handler: response,
    description: 'settings',
    tags: [ 'api', 'content', 'noTest' ],
    auth: 'token',
    validate: {
    },
    //response: { schema: commonSchemes.responseWrapper() }
  }
};
