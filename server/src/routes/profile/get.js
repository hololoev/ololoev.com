
const Joi = require('@hapi/joi');
const Op = require('sequelize').Op;

const userConst = require('../../constants/users');
const commonSchemes = require('../../libs/schemes/common');

const __editTimeout = 1000 * 60 * 15; // 15 min

async function response(request, h) {

  const posts = request.getModel(request.server.config.db.database, 'posts');
  const comments = request.getModel(request.server.config.db.database, 'comments');
  const communities = request.getModel(request.server.config.db.database, 'communities');

  let postCount = await posts.count({ where: { author_id: request.auth.artifacts.user.id } });

  let communityCount = await communities.count({ where: { author_id: request.auth.artifacts.user.id } });

  let postCommentsCount = await comments.count({
    include: [
      {
        model: posts,
        as: 'post',
        required: true,
        where: {
          author_id: request.auth.artifacts.user.id
        }
      }
    ]
  });

  let communityIDS = await communities.findAll({
    where: { author_id: request.auth.artifacts.user.id },
    attributes: [ 'id' ]
  });

  communityIDS = communityIDS.map( function(item) {
    return item.id;
  } );

  let communityPostCount = await posts.count({ where: { community_id: communityIDS } });
  let communityCommentsCount = await comments.count({
    include: [
      {
        model: posts,
        as: 'post',
        required: true,
        where: {
          community_id: communityIDS
        }
      }
    ]
  });

  let myPostsID = await posts.findAll({
    where: { author_id: request.auth.artifacts.user.id },
    attributes: [ 'id' ]
  });
  myPostsID = myPostsID.map( item => item.id );

  let allComments = await comments.findAll({
    limit: request.query.limit,
    offset: request.query.limit * (request.query.page-1),
    order: [ [ 'created_at', 'desc' ] ],
    where: {
      [ Op.or ]: [
        { post_id: myPostsID },
        { author_id: request.auth.artifacts.user.id }
      ]
    },
    include: [
      {
        model: posts,
        as: 'post',
        attributes: [ 'id', 'url', 'title' ]
      }
    ]
  });

  allComments = allComments.map(el => el.get({ plain: true }));

  //Check if comment editable
  for (let index=0; index<allComments.length; index++) {
    if ( allComments[ index ].author_id !== request.auth.artifacts.user.id ) {
      allComments[ index ].editable = false;
      continue;
    }

    let timeDiff = new Date() - allComments[ index ].created_at;
    if ( timeDiff < __editTimeout )
      allComments[ index ].editable = true;
    else
      allComments[ index ].editable = false;
  }

  let totalCount = await comments.count({ where: { post_id: myPostsID } });

  let totalPageCount = parseInt(totalCount / request.query.limit, 10);
  if ( (totalCount % request.query.limit) > 0 ) totalPageCount += 1;

  let page = {
    authorized: true,
    isAdmin: request.auth.artifacts.user.role <= userConst.roles.MODERATOR.id,
    user: request.auth.artifacts.user,
    page_url: '/profile',
    page_body: {
      postCount,
      communityCount,
      postCommentsCount,
      communityPostCount,
      communityCommentsCount,
      allComments
    },
    page_title: 'Ваш профиль',
    page_description: 'Ваш профиль',
    page_image: '',
    page_create_date: new Date(),
    page_meta: {
      query: request.query,
      totalCount: totalCount,
      totalPageCount: totalPageCount
    }
  };

  if ( (request.query.page - 1) >= 1 ) page.page_meta.prev_page = request.query.page - 1;
  if ( (request.query.page + 1) <= totalPageCount ) page.page_meta.next_page = request.query.page + 1;

  return h.view('profile', page, { layout: 'profile' });
}

const querySchema = commonSchemes.defaultPageQueryScheme.keys({
  commentsFilter: Joi.string().default('all')
});

module.exports = {
  method: 'GET',
  path: '/profile',
  options: {
    handler: response,
    description: 'User profile',
    tags: [ 'api', 'content', 'noTest' ],
    auth: 'token',
    validate: {
      query: querySchema
    },
    //response: { schema: commonSchemes.responseWrapper() }
  }
};
