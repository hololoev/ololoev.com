
const Op = require('sequelize').Op;

const userConst = require('../../../constants/users');
const commonSchemes = require('../../../libs/schemes/common');

async function response(request, h) {

  const { artifacts } = await request.server.auth.test('token', request);
  let user = artifacts.user;

  const communities = request.getModel(request.server.config.db.database, 'communities');
  const community_relations = request.getModel(request.server.config.db.database, 'community_relations');
  const sequelize = request.getDb().sequelize;

  let query = {
    limit: request.query.limit,
    offset: request.query.limit * (request.query.page-1),
    order: [ [ 'created_at', 'desc' ] ],
    where: {
      [ Op.and ]: [
        sequelize.literal(`communities.id in (SELECT cr.id FROM community_relations cr WHERE cr.user_id=${request.auth.artifacts.user.id})`)
      ]
    }
  };

  query.attributes = [ 'id', 'author_id', 'status', 'admin_status', 'admin_comment',
    'url', 'image_url', 'description' ,'title', 'excerpt', 'created_at', 'updated_at',
  ];

  let result = await communities.findAll(query);
  result = result.map( function(el) {
    let item = el.get({ plain: true });
    item.editable = false;

    if ( item.author_id === request.auth.artifacts.user.id )
      item.editable = true;

    return item;
  });

  let totalCount = await community_relations.count({ where: { user_id: user.id } });

  let totalPageCount = parseInt(totalCount / request.query.limit, 10);
  if ( (totalCount % request.query.limit) > 0 ) totalPageCount += 1;

  let page = {
    authorized: true,
    isAdmin: request.auth.artifacts.user.role <= userConst.roles.MODERATOR.id,
    user: request.auth.artifacts.user,
    page_url: '/profile/community',
    page_title: 'Мои сообщества',
    page_description: 'Мои сообщества',
    page_image: '',
    page_create_date: new Date(),
    page_body: result,
    page_meta: {
      query: request.query,
      totalCount: totalCount,
      totalPageCount: totalPageCount
    }
  };

  if ( (request.query.page - 1) >= 1 ) page.page_meta.prev_page = request.query.page - 1;
  if ( (request.query.page + 1) <= totalPageCount ) page.page_meta.next_page = request.query.page + 1;

  return h.view('profile-community-list', page, { layout: 'profile' });
}

module.exports = {
  method: 'GET',
  path: '/profile/community',
  options: {
    handler: response,
    description: 'Communities list',
    tags: [ 'api', 'content', 'noTest' ],
    auth: 'token',
    validate: {
      query: commonSchemes.defaultPageQueryScheme
    },
    //response: { schema: commonSchemes.responseWrapper() }
  }
};
