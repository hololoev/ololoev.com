
const userConst = require('../../../../constants/users');

async function response(request, h) {

  let page = {
    authorized: true,
    isAdmin: request.auth.artifacts.user.role <= userConst.roles.MODERATOR.id,
    user: request.auth.artifacts.user,
    page_url: '/profile/post/create',
    page_title: 'Новое сообщество',
    page_description: 'Новое сообщество',
    page_image: '',
    page_create_date: new Date(),
  };

  return h.view('profile-community-edit', page);
}

module.exports = {
  method: 'GET',
  path: '/profile/community/create',
  options: {
    handler: response,
    description: 'Create new community',
    tags: [ 'api', 'content', 'noTest' ],
    auth: 'token',
    validate: {
    },
    //response: { schema: commonSchemes.responseWrapper() }
  }
};
