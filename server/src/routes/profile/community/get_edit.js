
const Joi = require('@hapi/joi');

const userConst = require('../../../constants/users');

async function response(request, h) {

  let page = {
    authorized: true,
    isAdmin: request.auth.artifacts.user.role <= userConst.roles.MODERATOR.id,
    user: request.auth.artifacts.user,
    page_url: `/profile/community/${request.params.id}`,
    page_title: 'Редактор сообществ',
    page_description: 'Редактор сообществ',
    page_image: '',
    page_create_date: new Date(),
    page_body: {
      post: {}
    },
    page_meta: {
      communityId: request.params.id
    }
  };

  return h.view('profile-community-edit', page);
}

module.exports = {
  method: 'GET',
  path: '/profile/community/{id}',
  options: {
    handler: response,
    description: 'Create new community',
    tags: [ 'api', 'content', 'noTest' ],
    auth: 'token',
    validate: {
      params: Joi.object({
        id: Joi.number().integer().min(0).description('Community ID').example(1)
      })
    },
    //response: { schema: commonSchemes.responseWrapper() }
  }
};
