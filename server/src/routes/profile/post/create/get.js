
const userConst = require('../../../../constants/users');

async function response(request, h) {

  let page = {
    authorized: true,
    isAdmin: request.auth.artifacts.user.role <= userConst.roles.MODERATOR.id,
    user: request.auth.artifacts.user,
    page_url: '/profile/post/create',
    page_title: 'Новая публикация',
    page_description: 'Новая публикация',
    page_image: '',
    page_create_date: new Date(),
    page_body: {
      post: {}
    },
    page_meta: {
      postId: null
    }
  };

  return h.view('profile-post-edit', page, { layout: 'profile' });
}


module.exports = {
  method: 'GET',
  path: '/profile/post/create',
  options: {
    handler: response,
    description: 'Create new post',
    tags: [ 'api', 'content', 'noTest' ],
    auth: 'token',
    validate: {
    },
    //response: { schema: commonSchemes.responseWrapper() }
  }
};
