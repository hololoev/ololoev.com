
const Joi = require('@hapi/joi');

const userConst = require('../../../constants/users');

async function response(request, h) {

  let page = {
    authorized: true,
    isAdmin: request.auth.artifacts.user.role <= userConst.roles.MODERATOR.id,
    user: request.auth.artifacts.user,
    page_url: `/profile/post/${request.params.id}`,
    page_title: 'Редактирование публикации',
    page_description: 'Новая публикация',
    page_image: '',
    page_create_date: new Date(),
    page_body: {
      post: {}
    },
    page_meta: {
      postId: request.params.id
    }
  };

  return h.view('profile-post-edit', page, { layout: 'profile' });
}


module.exports = {
  method: 'GET',
  path: '/profile/post/{id}',
  options: {
    handler: response,
    description: 'Edit post',
    tags: [ 'api', 'content', 'noTest' ],
    auth: 'token',
    validate: {
      params: Joi.object({
        id: Joi.number().integer().min(0).description('Post ID').example(1)
      })
    },
    //response: { schema: commonSchemes.responseWrapper() }
  }
};
