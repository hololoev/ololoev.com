
const userConst = require('../../../constants/users');
const commonSchemes = require('../../../libs/schemes/common');

async function response(request, h) {

  const { artifacts } = await request.server.auth.test('token', request);
  let user = artifacts.user;

  const posts = request.getModel(request.server.config.db.database, 'posts');

  let query = {
    limit: request.query.limit,
    offset: request.query.limit * (request.query.page-1),
    order: [ [ 'created_at', 'desc' ] ],
    where: {
      author_id: user.id
    }
  };

  query.attributes = [ 'id', 'author_id', 'community_id', 'status', 'admin_status', 'admin_comment',
    'url', 'image_url', 'description' ,'title', 'excerpt', 'created_at', 'updated_at' ];

  query.include = [];

  let result = await posts.findAll(query);
  result = result.map(el => el.get({ plain: true }));

  let totalQuery = { where: { author_id: user.id } };
  let totalCount = await posts.count(totalQuery);

  let totalPageCount = parseInt(totalCount / request.query.limit, 10);
  if ( (totalCount % request.query.limit) > 0 ) totalPageCount += 1;

  let page = {
    authorized: true,
    isAdmin: request.auth.artifacts.user.role <= userConst.roles.MODERATOR.id,
    user: request.auth.artifacts.user,
    page_url: '/profile/post',
    page_title: 'Мои посты',
    page_description: 'Мои посты',
    page_image: '',
    page_create_date: new Date(),
    page_body: result,
    page_meta: {
      query: request.query,
      totalCount: totalCount,
      totalPageCount: totalPageCount
    }
  };

  if ( (request.query.page - 1) >= 1 ) page.page_meta.prev_page = request.query.page - 1;
  if ( (request.query.page + 1) <= totalPageCount ) page.page_meta.next_page = request.query.page + 1;

  return h.view('profile-post-list', page, { layout: 'profile' });
}

module.exports = {
  method: 'GET',
  path: '/profile/post',
  options: {
    handler: response,
    description: 'Post list',
    tags: [ 'api', 'content', 'noTest' ],
    auth: 'token',
    validate: {
      query: commonSchemes.defaultPageQueryScheme
    },
    //response: { schema: commonSchemes.responseWrapper() }
  }
};
