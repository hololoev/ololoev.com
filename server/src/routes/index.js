
const Joi = require('@hapi/joi');
const Op = require('sequelize').Op;

const commonSchemes = require('../libs/schemes/common');

const __defaultPageLimit = 10;
const __defaultPageNum = 1;

async function response(request, h) {
  let authorized = false;
  let user = false;

  try {
    const { artifacts } = await request.server.auth.test('token', request);
    authorized = true;
    user = artifacts.user;
  } catch {}

  const sequelize = request.getDb().sequelize;
  const posts = request.getModel(request.server.config.db.database, 'posts');

  let totalQuery;
  let query;

  if ( request.auth.isAuthenticated ) {
    totalQuery = {
      where: {
        status: posts.statuses.ENABLED.id,
        [ Op.or ]: [
          { community_id: { [ Op.ne ]: null } },
          {
            community_id: null,
            author_id: request.auth.artifacts.user.id
          }
        ]
      }
    };
    query = {
      where: {
        status: posts.statuses.ENABLED.id,
        [ Op.or ]: [
          { community_id: { [ Op.ne ]: null } },
          {
            community_id: null,
            author_id: request.auth.artifacts.user.id
          }
        ]
      },
      limit: request.query.limit,
      offset: request.query.limit * (request.query.page-1),
      order: [ [ 'published_at', 'DESC' ] ]
    };
  } else {
    totalQuery = {
      where: {
        status: posts.statuses.ENABLED.id,
        community_id: { [ Op.ne ]: null }
      }
    };
    query = {
      where: {
        status: posts.statuses.ENABLED.id,
        community_id: { [ Op.ne ]: null }
      },
      limit: request.query.limit,
      offset: request.query.limit * (request.query.page-1),
      order: [ [ 'published_at', 'DESC' ] ]
    };
  }

  query.attributes = [ 'id', 'author_id', 'community_id', 'status', 'admin_status', 'admin_comment',
    'url', 'image_url', 'description' ,'title', 'excerpt', 'published_at', 'created_at', 'updated_at',
    [ sequelize.literal(`(SELECT COUNT(*) FROM comments cmm WHERE cmm.post_id=posts.id AND cmm.admin_status>0)`), 'comments_count' ]
  ];

  query.include = [];

  let result = await posts.findAll(query);
  result = result.map(el => el.get({ plain: true }));
  let totalCount = await posts.count(totalQuery);

  let totalPageCount = parseInt(totalCount / request.query.limit, 10);
  if ( (totalCount % request.query.limit) > 0 ) totalPageCount += 1;

  let page = {
    authorized: authorized,
    user: user,
    page_url: '/',
    page_title: 'talks - главная',
    page_description: 'ололо лолол йа водитель нло!',
    page_image: '',
    page_create_date: new Date(),
    page_body: result,
    page_meta: {
      query: request.query,
      totalCount: totalCount,
      totalPageCount: totalPageCount
    }
  };

  if ( (request.query.page - 1) >= 1 ) page.page_meta.prev_page = request.query.page - 1;
  if ( (request.query.page + 1) <= totalPageCount ) page.page_meta.next_page = request.query.page + 1;

  return h.view('index', page);
}

module.exports = {
  method: 'GET',
  path: '/',
  options: {
    handler: response,
    description: 'Returns main page',
    tags: [ 'api', 'content', 'noTest' ],
    auth: {
      strategy: 'token',
      mode: 'try'
    },
    validate: {
      headers: commonSchemes.authHeaders,
      query: Joi.object({
        page: Joi.number().integer().default(__defaultPageNum).description('Page number').example(__defaultPageNum),
        limit: Joi.number().integer().default(__defaultPageLimit).description('Post count on the page').example(__defaultPageLimit)
      })
    },
    //response: { schema: commonSchemes.responseWrapper() }
  }
};
