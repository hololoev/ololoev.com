
async function response(request, h) {
  let authorized = false;
  let user = false;

  try {
    const { artifacts } = await request.server.auth.test('token', request);
    authorized = true;
    user = artifacts.user;
  } catch {}

  let page = {
    authorized: authorized,
    user: user,
    page_url: '/registration',
    page_title: 'Регистрация',
    page_description: 'регистрация нового пользователя',
    page_image: '',
    page_create_date: new Date(),
  };

  return h.view('registration', page);
}

module.exports = {
  method: 'GET',
  path: '/registration',
  options: {
    handler: response,
    description: 'Registration page',
    tags: [ 'api', 'content', 'noTest' ],
    auth: false,
    validate: {
    },
    //response: { schema: commonSchemes.responseWrapper() }
  }
};
