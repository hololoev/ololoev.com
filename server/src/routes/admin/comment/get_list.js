
const Boom = require('@hapi/boom');

const userConst = require('../../../constants/users');
const queryHelper = require('../../../libs/queryHelper');
const commonSchemes = require('../../../libs/schemes/common');

const __editTimeout = 1000 * 60 * 15; // 15 min

async function response(request, h) {

  const { artifacts } = await request.server.auth.test('token', request);
  let user = artifacts.user;

  if ( request.auth.artifacts.user.role > userConst.roles.MODERATOR.id ) throw Boom.forbidden('У Вас нет прав для просмотра этой страницы');

  const posts = request.getModel(request.server.config.db.database, 'posts');
  const comments = request.getModel(request.server.config.db.database, 'comments');

  let query = {
    limit: request.query.limit,
    offset: request.query.limit * (request.query.page-1),
    order: [ [ 'created_at', 'desc' ] ],
    where: {
      //author_id: user.id
    }
  };

  query.include = [
    {
      model: posts,
      as: 'post',
      attributes: [ 'id', 'url', 'title' ]
    }
  ];

  let result = await comments.findAll(query);
  result = result.map(el => el.get({ plain: true }));
  let totalCount = await comments.count(queryHelper.parseQueryParams({ where: { author_id: user.id } }));

  //Check if comment editable
  for (let index=0; index<result.length; index++) {
    let timeDiff = new Date() - result[ index ].created_at;
    if ( timeDiff < __editTimeout )
      result[ index ].editable = true;
    else
      result[ index ].editable = false;
  }

  let totalPageCount = parseInt(totalCount / request.query.limit);
  if ( (totalCount % request.query.limit) > 0 ) totalPageCount += 1;

  let page = {
    authorized: true,
    isAdmin: request.auth.artifacts.user.role <= userConst.roles.MODERATOR.id,
    user: request.auth.artifacts.user,
    page_url: '/admin/comment',
    page_title: 'Все комментарии комментарии',
    page_description: 'Все комментарии комментарии',
    page_image: '',
    page_create_date: new Date(),
    page_body: result,
    page_meta: {
      query: request.query,
      totalCount: totalCount,
      totalPageCount: totalPageCount
    }
  };

  if ( (request.query.page - 1) >= 1 ) page.page_meta.prev_page = request.query.page - 1;
  if ( (request.query.page + 1) <= totalPageCount ) page.page_meta.next_page = request.query.page + 1;

  return h.view('admin-comments-list.html', page, { layout: 'profile' });
}


module.exports = {
  method: 'GET',
  path: '/admin/comment',
  options: {
    handler: response,
    description: 'User comments',
    tags: [ 'api', 'content', 'noTest' ],
    auth: 'token',
    validate: {
      query: commonSchemes.defaultPageQueryScheme
    },
    //response: { schema: commonSchemes.responseWrapper() }
  }
};
