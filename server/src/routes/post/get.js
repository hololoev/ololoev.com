
const Joi = require('@hapi/joi');

const commonSchemes = require('../../libs/schemes/common');

const __defaultPageLimit = 100;
const __defaultPageNum = 1;

async function response(request, h) {
  let authorized = false;
  let user = false;

  try {
    const { artifacts } = await request.server.auth.test('token', request);
    authorized = true;
    user = artifacts.user;
  } catch {}

  const posts = request.getModel(request.server.config.db.database, 'posts');
  const users = request.getModel(request.server.config.db.database, 'users');
  const comments = request.getModel(request.server.config.db.database, 'comments');
  const communities = request.getModel(request.server.config.db.database, 'communities');

  let query = {
    where: {
      url: request.params.url
    }
  };

  //query.attributes = [ 'id', 'author_id', 'community_id', 'status', 'admin_status', 'admin_comment',
  //  'url', 'image_url', 'description' ,'title', 'excerpt', 'created_at', 'updated_at' ];

  query.include = [
    {
      model: users,
      as: 'author',
      attributes: [ 'id', 'name', 'role', 'status' ]
    },
    {
      model: communities,
      as: 'community',
      attributes: [ 'id', 'url', 'title', 'excerpt' ]
    }
  ];

  let result = await posts.findOne(query);
  result = result.get({ plain: true });

  let pageComments = await comments.findAll({
    where: {
      post_id: result.id
    },
    limit: request.query.limit,
    offset: request.query.limit * (request.query.page-1),
    order: [ ['created_at', 'asc'] ]
  });

  let totalCount = await comments.count({
    where: {
      post_id: result.id
    }
  });

  let totalPageCount = parseInt(totalCount / request.query.limit, 10);
  if ( (totalCount % request.query.limit) > 0 ) totalPageCount += 1;

  let page = {
    authorized: authorized,
    user: user,
    page_url: `/post/${request.params.url}`,
    page_title: result.title,
    page_description: result.description || result.excerpt || '',
    page_image: result.image_url || '/img/default-200x150.jpg',
    page_create_date: result.created_at,
    page_body: result,
    page_comments: pageComments.map(item => item.get({ plain: true })),
    page_meta: {
      query: request.query || null,
      totalCount: totalCount,
      totalPageCount: totalPageCount
    }
  };

  if ( (request.query.page - 1) >= 1 ) page.page_meta.prev_page = request.query.page - 1;
  if ( (request.query.page + 1) <= totalPageCount ) page.page_meta.next_page = request.query.page + 1;

  return h.view('post-view', page);
}

module.exports = {
  method: 'GET',
  path: '/post/{url}',
  options: {
    handler: response,
    description: 'Returns post page',
    tags: [ 'api', 'content', 'noTest' ],
    auth: false,
    validate: {
      headers: commonSchemes.authHeaders,
      query: Joi.object({
        page: Joi.number().integer().default(__defaultPageNum).description('Page number').example(__defaultPageNum),
        limit: Joi.number().integer().default(__defaultPageLimit).description('Post count on the page').example(__defaultPageLimit)
      }),
      params: Joi.object({
        url: Joi.string().max(255).required().example('olololo')
      })
    },
    //response: { schema: commonSchemes.responseWrapper() }
  }
};
