
const queryHelper = require('../../../../libs/queryHelper');
const basicSchemes = require('../../../../libs/schemes/basic');
const commonSchemes = require('../../../../libs/schemes/common');
const buildResult = require('../../../../libs/helpers').buildResult;

async function response(request) {
  const communities = request.getModel(request.server.config.db.database, 'communities');

  let query = queryHelper.parseQueryParams(request.query);

  query.attributes = [ 'id', 'type', 'author_id', 'status', 'admin_status',
    'admin_comment', 'url', 'image_url', 'description' ,'title', 'excerpt', 'created_at', 'updated_at' ];

  query.include = [];

  let result = await communities.findAll(query);
  result = result.map(el => el.get({ plain: true }));

  let totalCount = await communities.count(queryHelper.parseQueryParams(request.query));

  return buildResult(result, totalCount, result.length, request.query.__offset);
}

module.exports = {
  method: 'GET',
  path: '/api/pub/communities',
  options: {
    handler: response,
    description: 'Get communities list',
    tags: [ 'api' ],
    auth: false,
    validate: {
      headers: commonSchemes.authHeaders,
      query: commonSchemes.defaultQueryScheme
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.communitySchema) }
  }
};
