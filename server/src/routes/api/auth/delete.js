
const commonSchemes = require('../../../libs/schemes/common');
const buildResult = require('../../../libs/helpers').buildResult;

async function response(request) {
  const accessTokens = request.getModel(request.server.config.db.database, 'access_tokens');

  await accessTokens.destroy({
    where: {
      id: request.auth.artifacts.token.id
    }
  });

  return buildResult([]);
}


module.exports = {
  method: 'DELETE',
  path: '/api/auth',
  options: {
    handler: response,
    description: 'Destroy current auth token',
    tags: [ 'api', 'noTest' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders
    },
    response: { schema: commonSchemes.responseWrapper() }
  }
};
