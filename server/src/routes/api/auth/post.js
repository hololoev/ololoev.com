const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const basicSchemes = require('../../../libs/schemes/basic');
const commonSchemes = require('../../../libs/schemes/common');
const buildResult = require('../../../libs/helpers').buildResult;

async function response(request) {
  const accessTokens = request.getModel(request.server.config.db.database, 'access_tokens');
  const users = request.getModel(request.server.config.db.database, 'users');

  let userRecord = await users.findOne({ where: { name: request.payload.name } });
  if ( !userRecord ) throw Boom.unauthorized('Пользователь не найден, или неверный пароль');

  if ( !await users.verifyPassword(request.payload.password, userRecord.password) )
    throw Boom.unauthorized('Пользователь не найден, или неверный пароль');

  let token = await accessTokens.createAccessToken(userRecord);
  let result = await accessTokens.findAll({
    where: { id: token.id },
    include: [
      {
        model: users,
        as: 'user',
        attributes: users.allowedFields
      }
    ]
  });

  result = result.map(el => el.get({ plain: true }));
  return buildResult(result);
}

const responseScheme = basicSchemes.tokenScheme.keys({
  user: basicSchemes.userScheme
});

module.exports = {
  method: 'POST',
  path: '/api/auth',
  options: {
    handler: response,
    description: 'Create auth token',
    tags: [ 'api', 'noTest' ],
    validate: {
      payload: Joi.object({
        name: Joi.string().required().example('ololoev_i'),
        password: Joi.string().required().example('12345'),
        token: Joi.string().max(244).required().example('1245dfg5')
      })
    },
    response: { schema: commonSchemes.responseWrapper(responseScheme) }
  }
};
