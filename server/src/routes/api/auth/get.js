
const Boom = require('@hapi/boom');

const basicSchemes = require('../../../libs/schemes/basic');
const commonSchemes = require('../../../libs/schemes/common');
const buildResult = require('../../../libs/helpers').buildResult;

async function response(request) {
  const users = request.getModel(request.server.config.db.database, 'users');
  const accessTokens = request.getModel(request.server.config.db.database, 'access_tokens');

  let result = await accessTokens.findAll({
    where: { id: request.auth.artifacts.token.id },
    include: [
      {
        model: users,
        as: 'user',
        attributes: users.allowedFields
      }
    ]
  });

  if ( result.length === 0 )
    throw Boom.notFound('Token not found');

  result = result.map(el => el.get({ plain: true }));



  return buildResult(result);
}

const responseScheme = basicSchemes.tokenScheme.keys({
  user: basicSchemes.userScheme
});

module.exports = {
  method: 'GET',
  path: '/api/auth',
  options: {
    handler: response,
    description: 'Get current auth token',
    tags: [ 'api', 'noTest' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders
    },
    response: { schema: commonSchemes.responseWrapper(responseScheme) }
  }
};
