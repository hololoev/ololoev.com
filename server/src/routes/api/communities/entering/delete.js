
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const commonSchemes = require('../../../../libs/schemes/common');
const buildResult = require('../../../../libs/helpers').buildResult;

async function response(request) {
  const communities = request.getModel(request.server.config.db.database, 'communities');
  const community_relations = request.getModel(request.server.config.db.database, 'community_relations');

  let curCommunity = await communities.findOne({ where: { id: request.params.id } });
  if ( !curCommunity )
    throw Boom.notFound('Сообщество не найдено');

  let curRelation = await community_relations.findOne({
    where: {
      community_id: request.params.id,
      user_id: request.auth.artifacts.user.id
    }
  });

  if ( !curRelation )
    throw Boom.badRequest('Вы и так не в сообществе');

  await community_relations.destroy({
    where: {
      community_id: request.params.id,
      user_id: request.auth.artifacts.user.id
    }
  });

  return buildResult([]);
}

module.exports = {
  method: 'DELETE',
  path: '/api/communities/{id}/entering',
  options: {
    handler: response,
    description: 'Entering to community requets',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      params: Joi.object({
        id: Joi.number().integer().required().example(123)
      })
    },
    response: { schema: commonSchemes.responseWrapper() }
  }
};
