
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const editorHelper = require('../../../libs/editorHelper');
const basicSchemes = require('../../../libs/schemes/basic');
const commonSchemes = require('../../../libs/schemes/common');
const buildResult = require('../../../libs/helpers').buildResult;

async function response(request) {
  const users = request.getModel(request.server.config.db.database, 'users');
  const communities = request.getModel(request.server.config.db.database, 'communities');
  const community_relations = request.getModel(request.server.config.db.database, 'community_relations');

  if ( request.auth.artifacts.user.status === users.statuses.DISABLED.id )
    throw Boom.forbidden('Disabled user cannot modofy community');

  let curRelation = await community_relations.findOne({
    where: {
      community_id: request.params.id,
      user_id: request.auth.artifacts.user.id
    }
  });
  if ( !curRelation ) throw Boom.notFound('Вы не член этого сообщества');
  if ( curRelation.status !== community_relations.statuses.APPROVED.id )
    throw Boom.badRequest('Вы ещё не член этого сообщества');

  let curCommunity = await communities.findOne({ where: { id: request.params.id } });
  if ( !curCommunity ) throw Boom.notFound('Сообщество не найдено');

  // TODO Сейчас работает только AUTOCRACY, остальные типы сообществ будут много позже
  if ( curCommunity.author_id !== request.auth.artifacts.user.id )
    throw Boom.forbidden('Вы не можете изменять это сообщество');
  // ------

  let updCommunity = Object.assign({}, request.payload);

  updCommunity.body = editorHelper.replaceHeaders( editorHelper.mdConverter(request.payload.src_body) );
  updCommunity.updated_at = new Date();
  updCommunity.status = communities.statuses.ENABLED.id;

  await communities.update(updCommunity, { where: { id: request.params.id } });
  let result = await communities.findOne({ where: { id: request.params.id } });

  return buildResult([ result.get({ plain: true }) ]);
}

const requestScheme = Joi.object({
  image_url: Joi.string().max(255).allow(null, '').description('Post community preview').example('https://ololoev.com/img/test.jpg'),
  title: Joi.string().max(255).required().description('Post title').example('My new community'),
  excerpt: Joi.string().max(2048).required().allow(null, '').description('Small community description').example('Test excerpt'),
  src_body: Joi.string().required().description('Src community description').example(`# Hello world\n\n ## it's me!`),
  anon_comments_allow: Joi.number().integer().min(0).description('Allow or not anon comments').example(1),
  anon_comments_timeout: Joi.number().integer().min(0).description('Anon comment timeout(per ip)').example(1000 * 60),
  entry: Joi.number().integer().min(0).description('Entry type').example(1),
  non_community_comment_allow: Joi.number().integer().min(0).description('Allow or non community member comments').example(1),
  non_community_comment_timeout: Joi.number().integer().min(0).description('Non community member comment timeout').example(1000 * 60),
  community_comment_timeout: Joi.number().integer().min(0).description('Community member comment timeout').example(1000 * 60),
});

module.exports = {
  method: 'PUT',
  path: '/api/communities/{id}',
  options: {
    handler: response,
    description: 'Update community',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      params: Joi.object({
        id: Joi.number().integer().required().description('Community id').example(1)
      }),
      payload: requestScheme
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.communitySchema) }
  }
};
