
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const basicSchemes = require('../../../libs/schemes/basic');
const commonSchemes = require('../../../libs/schemes/common');
const buildResult = require('../../../libs/helpers').buildResult;

async function response(request) {
  const communities = request.getModel(request.server.config.db.database, 'communities');

  let result = await communities.findOne({ where: { id: request.params.id } });

  if ( !result ) throw Boom.notFound('Community not found');

  return buildResult([ result.get({ plain: true }) ]);
}

module.exports = {
  method: 'GET',
  path: '/api/communities/{id}',
  options: {
    handler: response,
    description: 'Get community by id',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      params: Joi.object({
        id: Joi.number().integer().required().description('Community id').example(1)
      })
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.communitySchema) }
  }
};
