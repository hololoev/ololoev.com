
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const editorHelper = require('../../../libs/editorHelper');
const basicSchemes = require('../../../libs/schemes/basic');
const commonSchemes = require('../../../libs/schemes/common');
const buildResult = require('../../../libs/helpers').buildResult;

async function response(request) {
  const users = request.getModel(request.server.config.db.database, 'users');
  const communities = request.getModel(request.server.config.db.database, 'communities');
  const community_relations = request.getModel(request.server.config.db.database, 'community_relations');

  if ( request.auth.artifacts.user.status === users.statuses.DISABLED.id )
    throw Boom.forbidden('Disabled user cannot create community');

  let newCommunity = Object.assign({}, request.payload);

  newCommunity.body = editorHelper.replaceHeaders( editorHelper.mdConverter(request.payload.src_body) );
  newCommunity.created_at = new Date();
  newCommunity.updated_at = new Date();
  newCommunity.author_id = request.auth.artifacts.user.id;
  newCommunity.status = communities.statuses.ENABLED.id;
  newCommunity.admin_status = communities.statuses.ENABLED.id;
  newCommunity.url = await editorHelper.generateUrl(request.payload.title, communities);

  let newItem = await communities.create(newCommunity);
  let result = await communities.findOne({ where: { id: newItem.id } });

  await community_relations.create({
    community_id: result.id,
    user_id: request.auth.artifacts.user.id,
    status: community_relations.statuses.APPROVED.id
  });

  return buildResult([ result.get({ plain: true }) ]);
}

const requestScheme = Joi.object({
  type: Joi.number().integer().min(0).required().description('Commynity type').example(1),
  image_url: Joi.string().max(255).allow(null, '').description('Post community preview').example('https://ololoev.com/img/test.jpg'),
  title: Joi.string().max(255).required().description('Post title').example('My new community'),
  excerpt: Joi.string().max(2048).required().allow(null, '').description('Small community description').example('Test excerpt'),
  src_body: Joi.string().required().description('Src community description').example(`# Hello world\n\n ## it's me!`),
  anon_comments_allow: Joi.number().integer().min(0).description('Allow or not anon comments').example(1),
  anon_comments_timeout: Joi.number().integer().min(0).description('Anon comment timeout(per ip)').example(1000 * 60),
  entry: Joi.number().integer().min(0).description('Entry type').example(1),
  non_community_comment_allow: Joi.number().integer().min(0).description('Allow or non community member comments').example(1),
  non_community_comment_timeout: Joi.number().integer().min(0).description('Non community member comment timeout').example(1000 * 60),
  community_comment_timeout: Joi.number().integer().min(0).description('Community member comment timeout').example(1000 * 60),
});

module.exports = {
  method: 'POST',
  path: '/api/communities',
  options: {
    handler: response,
    description: 'Create new community',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      payload: requestScheme
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.communitySchema) }
  }
};
