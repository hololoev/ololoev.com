
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const basicSchemes = require('../../../libs/schemes/basic');
const commonSchemes = require('../../../libs/schemes/common');
const buildResult = require('../../../libs/helpers').buildResult;


async function response(request) {
  const comments = request.getModel(request.server.config.db.database, 'comments');

  let result = await comments.findOne({
    where: { id: request.params.id }
  });

  if ( !result )
    throw Boom.notFound('Комментарий не найден');

  return buildResult([ result.dataValues ]);
}

module.exports = {
  method: 'GET',
  path: '/api/comments/{id}',
  options: {
    handler: response,
    description: 'Returns comment by id',
    tags: [ 'api' ],
    auth: {
      mode: 'try',
      strategy: 'token'
    },
    validate: {
      headers: commonSchemes.authHeaders,
      params: Joi.object({
        id: Joi.number().integer().required().description('Comment ID').example(1)
      })
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.commentSchema) }
  }
};
