
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const editorHelper = require('../../../libs/editorHelper');
const basicSchemes = require('../../../libs/schemes/basic');
const commonSchemes = require('../../../libs/schemes/common');
const buildResult = require('../../../libs/helpers').buildResult;

const __minBodyLength = 1;
const __editTimeout = 1000 * 60 * 15; // 15 min

async function response(request) {
  const comments = request.getModel(request.server.config.db.database, 'comments');

  let curComment = await comments.findOne({ where: { id: request.params.id } });

  if ( !curComment )
    throw Boom.notFound('Комментарий не найден');

  if ( curComment.author_id !== request.auth.artifacts.user.id )
    throw Boom.forbidden('Вы не можете редактировать чужой комментарий');

  if ( (new Date() - curComment.created_at) > __editTimeout )
    throw Boom.forbidden('Вы не можете редактировать старий комментарий');

  let updComment = Object.assign({}, request.payload);

  updComment.body = editorHelper.replaceHeaders( editorHelper.mdConverter(request.payload.src_body) );
  updComment.updated_at = new Date();

  await comments.update(updComment, { where: { id: request.params.id } });
  let result = await comments.findOne({ where: { id: request.params.id } });

  return buildResult([ result.dataValues ]);
}

const requestScheme = Joi.object({
  src_body: Joi.string().min(__minBodyLength).description('Comment src').example('My new comment')
});

module.exports = {
  method: 'PUT',
  path: '/api/comments/{id}',
  options: {
    handler: response,
    description: 'Create new comment',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      payload: requestScheme,
      params: Joi.object({
        id: Joi.number().integer().required().description('Comment ID').example(1)
      })
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.commentSchema) }
  }
};
