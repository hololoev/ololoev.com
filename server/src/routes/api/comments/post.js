
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const usersConsts = require('../../../constants/users');
const editorHelper = require('../../../libs/editorHelper');
const basicSchemes = require('../../../libs/schemes/basic');
const commonSchemes = require('../../../libs/schemes/common');
const buildResult = require('../../../libs/helpers').buildResult;

const __signLength = 8;
const __minBodyLength = 1;

function toString(val) {
  if ( typeof(val) === 'undefined' ) return '';
  if ( val === null ) return '';

  return val.toString();
}

function int32ToBytes(num) {
  let arr = new Uint8Array([
    (num & 0xff000000) >> 24,
    (num & 0x00ff0000) >> 16,
    (num & 0x0000ff00) >> 8,
    (num & 0x000000ff)
  ]);
  return arr;
}

function getCharValue(char) {
  let bytes = int32ToBytes(char.charCodeAt(0));
  let result = new Uint8Array([ 0 ]);

  for (let index=0; index<bytes.byteLength; index++)
    result[ 0 ] = result[ 0 ] ^ bytes[ index ];

  return result;
}

function sign(srcString, length) {
  let result = new Uint8Array(length);
  let index = 0;
  let randomizer = new Uint8Array([ 0 ]);

  for (let char of srcString) {
    let charVal = getCharValue(char);
    result[ index ] = result[ index ] ^ charVal[ 0 ] ^ randomizer[ 0 ];
    randomizer[ 0 ] = randomizer[ 0 ] ^ result[ index ];

    index++;
    if ( index >= result.byteLength) index = 0;
  }

  let resultString = '';
  for (index=0; index<result.byteLength; index++)
    resultString += result[ index ].toString(16).padStart(2, '0');

  return resultString;
}

async function response(request) {
  const users = request.getModel(request.server.config.db.database, 'users');
  const posts = request.getModel(request.server.config.db.database, 'posts');
  const comments = request.getModel(request.server.config.db.database, 'comments');
  const communities = request.getModel(request.server.config.db.database, 'communities');

  let token = null;
  if ( request.auth.isAuthenticated ) token = request.auth.artifacts.token.token;
  let srcString = toString(request.payload.post_id) + toString(request.payload.author_name) + toString(request.payload.src_body) + toString(token);
  let signature = sign(srcString, __signLength);

  if ( request.payload.signature !== signature )
    throw Boom.forbidden('Неправильная подпись');

  let curPost = await posts.findOne({ where: { id: request.payload.post_id } });
  if ( !curPost )
    throw Boom.notFound('Пост не найден');

  if ( curPost.admin_status === posts.statuses.DISABLED.id )
    throw Boom.forbidden('Пост заблокирован администратором');

  let curCommunity = await communities.findOne({ where: { id: curPost.community_id } });
  if ( !curCommunity ) {
    if ( !request.auth.isAuthenticated )
      throw Boom.forbidden('Анонимные комментарии в постах без комьюнити запрещены');

    if ( request.auth.artifacts.user.role > usersConsts.roles.MODERATOR.id )
      throw Boom.forbidden('Вы не можете оставлять комментарии в посте без сообщества');
  }

  if ( curCommunity.admin_status === communities.statuses.DISABLED.id )
    throw Boom.forbidden('Сообщество заблокирован администратором');

  if ( (curCommunity.anon_comments_allow === communities.commentStatuses.DISALLOW.id) && !request.auth.isAuthenticated )
    throw Boom.forbidden('Анонимные комментарии запрещены в этом сообществе');

  if ( (curCommunity.public_type === communities.publicTypes.PRIVATE.id) && !request.auth.isAuthenticated )
    throw Boom.forbidden('Анонимные комментарии запрещены в закрытом сообществе');

  if ( (curCommunity.non_community_comment_allow === communities.commentStatuses.DISALLOW.id) && !request.auth.isAuthenticated )
    throw Boom.forbidden('В этом сообществе комментить могут только его члены');

  //TODO ещё проверить состоит ли юзер в сообществе TODO

  let newComment = {
    post_id: request.payload.post_id,
    src_body: request.payload.src_body,
    src_ip_address: request.headers[ 'x-real-ip' ] || request.headers[ 'x-forwarded-for' ]
  };

  if ( !request.auth.isAuthenticated ) {
    newComment.author_name = request.payload.author_name;

    let lasAnonComment = await comments.findOne({ where: {
      author_id: null,
      post_id: request.payload.post_id,
      src_ip_address: request.headers[ 'x-real-ip' ] || request.headers[ 'x-forwarded-for' ]
    }, order: [ [ 'created_at', 'DESC' ] ] });

    if ( lasAnonComment ) {
      let timeDiff = (new Date() /1) - (lasAnonComment.created_at/1);

      if ( timeDiff < curCommunity.anon_comments_timeout )
        throw Boom.forbidden(`Таймаут на анонимное комментирование с Вашего IP адреса не пройден.`);
    }
  } else
    newComment.author_name = request.auth.artifacts.user.name;

  newComment.body = editorHelper.replaceHeaders( editorHelper.mdConverter(request.payload.src_body) );
  newComment.created_at = new Date();
  newComment.updated_at = new Date();

  if ( request.auth.isAuthenticated ) {
    if ( request.auth.artifacts.user.status === users.statuses.DISABLED.id )
      throw Boom.forbidden('Выключенный ползователь не может комментить');

    newComment.author_id = request.auth.artifacts.user.id;
    newComment.author_name = request.auth.artifacts.user.name;
  }

  let newDBComment = await comments.create(newComment);
  return buildResult([ newDBComment.get({ plain: true }) ]);
}

const requestScheme = Joi.object({
  post_id: Joi.number().integer().description('Post ID').example(1).default(1),
  author_name: Joi.string().max(255).allow(null, '').description('User name').example('Petrov'),
  src_body: Joi.string().min(__minBodyLength).description('Comment src').example('My new comment'),
  signature: Joi.string().description('Signature').example('aabbccdd')
});

module.exports = {
  method: 'POST',
  path: '/api/comments',
  options: {
    handler: response,
    description: 'Create new comment',
    tags: [ 'api' ],
    auth: {
      mode: 'try',
      strategy: 'token'
    },
    validate: {
      headers: commonSchemes.authHeaders,
      payload: requestScheme
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.commentSchema) }
  }
};
