
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const basicSchemes = require('../../../../libs/schemes/basic');
const commonSchemes = require('../../../../libs/schemes/common');
const buildResult = require('../../../../libs/helpers').buildResult;

async function response(request) {
  const users = request.getModel(request.server.config.db.database, 'users');

  let curUser = await users.findOne({ where: { id: request.auth.artifacts.user.id } });
  if ( !curUser ) throw Boom.notFound('Пользователь не существует');

  if ( !await users.verifyPassword(request.payload.old_password, curUser.password) )
    throw Boom.unauthorized('Старый пароль не верный');

  if ( request.payload.new_password !== request.payload.confirmation )
    throw Boom.badRequest('Пароль и подтверждение не совпадают');

  await users.update({
    password: request.payload.password,
    updated_at: new Date()
  }, { where: { id: request.auth.artifacts.user.id } });

  let result = await users.findOne({
    where: { id: request.auth.artifacts.user.id },
    attributes: users.allowedFields
  });

  return buildResult([ result.get({ plain: true }) ]);
}

const requestScheme = Joi.object({
  old_password: Joi.string().required().example('12345'),
  new_password: Joi.string().required().example('12345'),
  confirmation: Joi.string().required().example('12345')
});

module.exports = {
  method: 'PUT',
  path: '/api/profile/password',
  options: {
    handler: response,
    description: 'Update user`s password',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      payload: requestScheme
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.userScheme) }
  }
};
