
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const basicSchemes = require('../../../../libs/schemes/basic');
const commonSchemes = require('../../../../libs/schemes/common');
const buildResult = require('../../../../libs/helpers').buildResult;

async function response(request) {
  const users = request.getModel(request.server.config.db.database, 'users');

  let curUser = await users.findOne({ where: { id: request.auth.artifacts.user.id } });
  if ( !curUser ) throw Boom.notFound('Пользователь не существует');

  let updateData = Object.assign({}, request.payload);
  updateData.updated_at = new Date();

  if ( updateData.email !== curUser.email )
    updateData.email_verified = null;

  await users.update(updateData, { where: { id: request.auth.artifacts.user.id } });

  let result = await users.findOne({
    where: { id: request.auth.artifacts.user.id },
    attributes: users.allowedFields
  });

  return buildResult([ result.get({ plain: true }) ]);
}

const requestScheme = Joi.object({
  email: Joi.string().description('User email').example('pupkin@ololo.com'),
  avatar_url: Joi.string().description('Avatar img url').example('http://ololo.com/img/test.img'),
  user_self_description: Joi.string().description('User self description').example('Ololo, ololo, ya voditel NLO'),
}).unknown(false);

module.exports = {
  method: 'PUT',
  path: '/api/profile/settings',
  options: {
    handler: response,
    description: 'Update user`s settings',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      payload: requestScheme
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.userScheme) }
  }
};
