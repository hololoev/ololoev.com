const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const basicSchemes = require('../../../libs/schemes/basic');
const commonSchemes = require('../../../libs/schemes/common');
const buildResult = require('../../../libs/helpers').buildResult;

async function response(request) {
  const users = request.getModel(request.server.config.db.database, 'users');

  if ( !request.payload.terms ) throw Boom.badRequest('Вы обязаны принять условия использования');

  let userRecord = await users.findOne({ where: { name: request.payload.name } });
  if ( userRecord ) throw Boom.badRequest('Имя пользователя уже занято');

  if ( request.payload.email ) {
    let userByEmail = await users.findOne({ where: { email: request.payload.email } });
    if ( userByEmail ) throw Boom.badRequest('Email уже занят другим пользователем');
  }

  if ( request.payload.password !== request.payload.confirmation )
    throw Boom.badRequest('Пароль и подтверждение не совпадают');

  let newUser = {
    name: request.payload.name,
    email: request.payload.email,
    password: request.payload.password,
    status: users.statuses.ENABLED.id,
    role: users.roles.USER.id,
    created_at: new Date(),
    updated_at: new Date()
  };

  let newDBUser = await users.create(newUser);

  let result = newDBUser.dataValues;
  delete result.password;

  return buildResult([ result ]);
}

module.exports = {
  method: 'POST',
  path: '/api/registration',
  options: {
    handler: response,
    description: 'Register new user',
    tags: [ 'api', 'noTest' ],
    validate: {
      headers: commonSchemes.authHeaders,
      payload: Joi.object({
        name: Joi.string().required().min(1).max(255).description('User name').example('ololoev'),
        email: Joi.string().allow('', null).max(255).description('User email, not required').example('pupkin@gmail.com'),
        password: Joi.string().required().description('Account password').example('12345'),
        confirmation: Joi.string().required().description('Password confirmation').example('12345'),
        terms: Joi.boolean().required().description('true/false accepted terms or not').example(true),
        token: Joi.string().required().max(255).description('Request confirmation token').example('ololo ololo ya voditel nlo')
      })
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.userScheme) }
  }
};
