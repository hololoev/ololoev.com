
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const editorHelper = require('../../../libs/editorHelper');
const basicSchemes = require('../../../libs/schemes/basic');
const commonSchemes = require('../../../libs/schemes/common');
const buildResult = require('../../../libs/helpers').buildResult;

async function response(request) {
  const users = request.getModel(request.server.config.db.database, 'users');
  const categories = request.getModel(request.server.config.db.database, 'categories');

  if ( request.auth.artifacts.user.role > users.roles.ADMIN.id )
    throw Boom.forbidden('You cannot change category');

  if ( request.auth.artifacts.user.status === users.statuses.DISABLED.id )
    throw Boom.forbidden('Disabled user cannot create community');

  let newCommunity = Object.assign({}, request.payload);

  newCommunity.body = editorHelper.replaceHeaders( editorHelper.mdConverter(request.payload.src_body) );
  newCommunity.created_at = new Date();
  newCommunity.updated_at = new Date();
  newCommunity.author_id = request.auth.artifacts.user.id;
  newCommunity.status = categories.statuses.ENABLED.id;
  newCommunity.admin_status = categories.statuses.ENABLED.id;
  newCommunity.url = await editorHelper.generateUrl(request.payload.title, categories);

  let newItem = await categories.create(newCommunity);
  let result = await categories.findOne({ where: { id: newItem.id } });

  return buildResult([ result.get({ plain: true }) ]);
}

const requestScheme = Joi.object({
  status: Joi.number().integer().description('Publication status').default(1).example(1),
  image_url: Joi.string().max(255).allow(null, '').description('Post category preview').example('https://ololoev.com/img/test.jpg'),
  title: Joi.string().max(255).required().description('Post title').example('My new category'),
  excerpt: Joi.string().max(2048).required().allow(null, '').description('Small category description').example('Test excerpt'),
  src_body: Joi.string().required().description('Src category description').example(`# Hello world\n\n ## it's me!`),
});

module.exports = {
  method: 'POST',
  path: '/api/categories',
  options: {
    handler: response,
    description: 'Create new category',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      payload: requestScheme
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.categorySchema) }
  }
};
