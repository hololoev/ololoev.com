
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const editorHelper = require('../../../libs/editorHelper');
const basicSchemes = require('../../../libs/schemes/basic');
const commonSchemes = require('../../../libs/schemes/common');
const buildResult = require('../../../libs/helpers').buildResult;

async function response(request) {
  const users = request.getModel(request.server.config.db.database, 'users');
  const categories = request.getModel(request.server.config.db.database, 'categories');

  if ( request.auth.artifacts.user.role > users.roles.ADMIN.id )
    throw Boom.forbidden('You cannot change category');

  if ( request.auth.artifacts.user.status === users.statuses.DISABLED.id )
    throw Boom.forbidden('Disabled user cannot modofy community');

  let curCategory = await categories.findOne({ where: { id: request.params.id } });
  if ( !curCategory ) throw Boom.notFound('Category not found');
  // ------

  let updCategory = Object.assign({}, request.payload);

  updCategory.body = editorHelper.replaceHeaders( editorHelper.mdConverter(request.payload.src_body) );
  updCategory.updated_at = new Date();
  updCategory.status = categories.statuses.ENABLED.id;

  await categories.update(updCategory, { where: { id: request.params.id } });
  let result = await categories.findOne({ where: { id: request.params.id } });

  return buildResult([ result.get({ plain: true }) ]);
}

const requestScheme = Joi.object({
  status: Joi.number().integer().description('Publication status').default(1).example(1),
  image_url: Joi.string().max(255).allow(null, '').description('Post category preview').example('https://ololoev.com/img/test.jpg'),
  title: Joi.string().max(255).required().description('Post title').example('My new category'),
  excerpt: Joi.string().max(2048).required().allow(null, '').description('Small category description').example('Test excerpt'),
  src_body: Joi.string().required().description('Src category description').example(`# Hello world\n\n ## it's me!`),
});

module.exports = {
  method: 'PUT',
  path: '/api/categories/{id}',
  options: {
    handler: response,
    description: 'Update category',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      params: Joi.object({
        id: Joi.number().integer().required().description('Category id').example(1)
      }),
      payload: requestScheme
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.communitySchema) }
  }
};
