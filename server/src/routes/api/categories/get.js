
const queryHelper = require('../../../libs/queryHelper');
const basicSchemes = require('../../../libs/schemes/basic');
const commonSchemes = require('../../../libs/schemes/common');
const buildResult = require('../../../libs/helpers').buildResult;

async function response(request) {
  const categories = request.getModel(request.server.config.db.database, 'categories');

  let query = queryHelper.parseQueryParams(request.query);

  query.attributes = [ 'id', 'author_id', 'status', 'url', 'image_url', 'description' ,'title', 'excerpt', 'created_at', 'updated_at' ];

  query.include = [];

  let result = await categories.findAll(query);
  result = result.map(el => el.get({ plain: true }));

  let totalCount = await categories.count(queryHelper.parseQueryParams(request.query));

  return buildResult(result, totalCount, result.length, request.query.__offset);
}

module.exports = {
  method: 'GET',
  path: '/api/categories',
  options: {
    handler: response,
    description: 'Get categories list',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      query: commonSchemes.defaultQueryScheme
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.categorySchema) }
  }
};
