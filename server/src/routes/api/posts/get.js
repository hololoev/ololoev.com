
const Joi = require('@hapi/joi');

const queryHelper = require('../../../libs/queryHelper');
const basicSchemes = require('../../../libs/schemes/basic');
const commonSchemes = require('../../../libs/schemes/common');
const buildResult = require('../../../libs/helpers').buildResult;

async function response(request) {
  const posts = request.getModel(request.server.config.db.database, 'posts');
  const categories = request.getModel(request.server.config.db.database, 'categories');
  const category_relations = request.getModel(request.server.config.db.database, 'category_relations');

  let query = queryHelper.parseQueryParams(request.query);

  query.where.author_id = request.auth.artifacts.user.id;
  query.attributes = [ 'id', 'author_id', 'community_id', 'status', 'admin_status', 'admin_comment',
    'url', 'image_url', 'description' ,'title', 'excerpt', 'created_at', 'updated_at' ];

  query.include = [
    {
      model: category_relations,
      as: 'category_relations',
      attributes: [ 'id', 'post_id', 'category_id' ],
      include: [
        { model: categories, as: 'category', attributes: [ 'id', 'title' ], }
      ]
    }
  ];

  let result = await posts.findAll(query);
  result = result.map(el => el.get({ plain: true }));
  let totalCount = await posts.count(queryHelper.parseQueryParams(request.query));

  return buildResult(result, totalCount, result.length, request.query.__offset);
}

const responseSchema = basicSchemes.postSchema.keys({
  category_relations: Joi.array().items(basicSchemes.categotyRelationsSchema.keys({
    category: basicSchemes.categorySchema
  }))
});

module.exports = {
  method: 'GET',
  path: '/api/posts',
  options: {
    handler: response,
    description: 'Get post list',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      query: commonSchemes.defaultQueryScheme
    },
    response: { schema: commonSchemes.responseWrapper(responseSchema) }
  }
};
