
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const basicSchemes = require('../../../libs/schemes/basic');
const commonSchemes = require('../../../libs/schemes/common');
const buildResult = require('../../../libs/helpers').buildResult;

async function response(request) {
  const posts = request.getModel(request.server.config.db.database, 'posts');
  const categories = request.getModel(request.server.config.db.database, 'categories');
  const category_relations = request.getModel(request.server.config.db.database, 'category_relations');

  let result = await posts.findOne({
    where: { id: request.params.id },
    include: [
      {
        model: category_relations,
        as: 'category_relations',
        attributes: [ 'id', 'post_id', 'category_id' ],
        include: [
          { model: categories, as: 'category', attributes: [ 'id', 'title' ], }
        ]
      }
    ]
  });

  if ( !result ) throw Boom.notFound('Post not found');

  return buildResult([ result.get({ plain: true }) ]);
}

const responseSchema = basicSchemes.postSchema.keys({
  category_relations: Joi.array().items(basicSchemes.categotyRelationsSchema.keys({
    category: basicSchemes.categorySchema
  }))
});

module.exports = {
  method: 'GET',
  path: '/api/posts/{id}',
  options: {
    handler: response,
    description: 'Get post by id',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      params: Joi.object({
        id: Joi.number().integer().required().description('Post id').example(1)
      })
    },
    response: { schema: commonSchemes.responseWrapper(responseSchema) }
  }
};
