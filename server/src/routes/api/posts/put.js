
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const editorHelper = require('../../../libs/editorHelper');
const basicSchemes = require('../../../libs/schemes/basic');
const commonSchemes = require('../../../libs/schemes/common');
const buildResult = require('../../../libs/helpers').buildResult;

async function response(request) {
  const users = request.getModel(request.server.config.db.database, 'users');
  const posts = request.getModel(request.server.config.db.database, 'posts');
  const categories = request.getModel(request.server.config.db.database, 'categories');
  const category_relations = request.getModel(request.server.config.db.database, 'category_relations');

  if ( request.auth.artifacts.user.status === users.statuses.DISABLED.id )
    throw Boom.forbidden('Disabled user cannot modofy post');

  let curPost = await posts.findOne({ where: { id: request.params.id } });
  if ( !curPost ) throw Boom.notFound('Community not found');

  if ( curPost.author_id !== request.auth.artifacts.user.id )
    throw Boom.forbidden('You cannot modify this post');

  let updPost = Object.assign({}, request.payload);

  if ( request.payload.status && (request.payload.status === posts.statuses.ENABLED.id) )
    if ( !curPost.published_at && (curPost.status === posts.statuses.DISABLED.id) )
      updPost.published_at = new Date();

  updPost.body = editorHelper.replaceHeaders( editorHelper.mdConverter(request.payload.src_body) );
  updPost.updated_at = new Date();

  await posts.update(updPost, { where: { id: request.params.id } });

  // THEN update categories
  await category_relations.destroy({ where: { post_id: request.params.id } });
  for (let category_id of request.payload.categories)
    await category_relations.create({
      post_id: request.params.id,
      category_id: category_id,
      created_at: new Date(),
      updated_at: new Date()
    });

  let result = await posts.findOne({
    where: { id: request.params.id },
    include: [
      {
        model: category_relations,
        as: 'category_relations',
        attributes: [ 'id', 'post_id', 'category_id' ],
        include: [
          { model: categories, as: 'category', attributes: [ 'id', 'title' ], }
        ]
      }
    ]
  });

  return buildResult([ result.get({ plain: true }) ]);
}

const requestScheme = Joi.object({
  status: Joi.number().integer().description('Post status').example(1),
  title: Joi.string().max(255).description('Post title').example('My new post'),
  community_id: Joi.number().integer().allow(null).description('Community id').example(null),
  image_url: Joi.string().max(255).allow(null, '').description('Post small preview').example('https://ololoev.com/img/test.jpg'),
  excerpt: Joi.string().max(2048).allow(null, '').description('Small post description').example('Test excerpt'),
  src_body: Joi.string().description('Src post pody').example(`# Hello world\n\n ## it's me!`),
  categories: Joi.array().items(Joi.number().integer()).description('Post categories').example([1,2,3])
});

const responseSchema = basicSchemes.postSchema.keys({
  category_relations: Joi.array().items(basicSchemes.categotyRelationsSchema.keys({
    category: basicSchemes.categorySchema
  }))
});

module.exports = {
  method: 'PUT',
  path: '/api/posts/{id}',
  options: {
    handler: response,
    description: 'Update post',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      params: Joi.object({
        id: Joi.number().integer().required().description('Post id').example(1)
      }),
      payload: requestScheme
    },
    response: { schema: commonSchemes.responseWrapper(responseSchema) }
  }
};
