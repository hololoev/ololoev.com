
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const basicSchemes = require('../../../libs/schemes/basic');
const commonSchemes = require('../../../libs/schemes/common');
const buildResult = require('../../../libs/helpers').buildResult;

async function response(request) {
  const users = request.getModel(request.server.config.db.database, 'users');

  if ( request.auth.artifacts.user.role > users.roles.ADMIN.id )
    throw Boom.forbidden();

  let query = {
    where: { id: request.params.id },
    attributes: [ 'id', 'name', 'email', 'role', 'status', 'updatedAt', 'createdAt' ]
  };

  let result = await users.findAll(query);
  result = result.map(el => el.get({ plain: true }));

  return buildResult(result);
}

module.exports = {
  method: 'GET',
  path: '/api/users/{id}',
  options: {
    handler: response,
    description: 'Get user by id',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      params: Joi.object({
        id: Joi.number().integer().required().description('User id').example(1)
      })
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.userScheme) }
  }
};
