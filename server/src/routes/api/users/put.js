
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const userConst = require('../../../constants/users');
const basicSchemes = require('../../../libs/schemes/basic');
const commonSchemes = require('../../../libs/schemes/common');
const buildResult = require('../../../libs/helpers').buildResult;

async function response(request) {
  const users = request.getModel(request.server.config.db.database, 'users');

  if ( request.auth.artifacts.user.role > users.roles.ADMIN.id )
    throw Boom.forbidden();

  if ( request.params.role <= request.auth.artifacts.user.role )
    throw Boom.forbidden();

  let curItem = await users.findOne({ where: { id: request.params.id } });
  if ( !curItem ) throw Boom.notFound();

  await users.update(request.payload, { where: { id: request.params.id } });
  let result = await users.findOne({
    where: { id: request.params.id },
    attributes: [ 'id', 'name', 'email', 'role', 'status', 'updatedAt', 'createdAt' ]
  });

  return buildResult([result.get({ plain: true })]);
}

const requestScheme = Joi.object({
  status: Joi.number().integer().default(userConst.statuses.ENABLED.id).description('User status').example(1),
  role: Joi.number().integer().default(userConst.roles.USER.id).description('User role').example(2),
  email: Joi.string().description('Email').required().example('ololoev@gmail.com'),
  name: Joi.string().description('Name').required().example('Ololoev I')
});

module.exports = {
  method: 'PUT',
  path: '/api/users/{id}',
  options: {
    handler: response,
    description: 'Update user',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      payload: requestScheme,
      params: Joi.object({
        id: Joi.number().integer().description('User id').example(1)
      })
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.userScheme) }
  }
};
