
const Boom = require('@hapi/boom');

const queryHelper = require('../../../libs/queryHelper');
const basicSchemes = require('../../../libs/schemes/basic');
const commonSchemes = require('../../../libs/schemes/common');
const buildResult = require('../../../libs/helpers').buildResult;

async function response(request) {
  const users = request.getModel(request.server.config.db.database, 'users');

  if ( request.auth.artifacts.user.role > users.roles.ADMIN.id )
    throw Boom.forbidden();

  let query = queryHelper.parseQueryParams(request.query);
  query.attributes = [ 'id', 'name', 'email', 'role', 'status', 'updatedAt', 'createdAt' ];

  let result = await users.findAll(query);
  result = result.map(el => el.get({ plain: true }));
  let totalCount = await users.count(query);

  return buildResult(result, totalCount, result.length, request.query.__offset);
}

module.exports = {
  method: 'GET',
  path: '/api/users',
  options: {
    handler: response,
    description: 'Get users list',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      query: commonSchemes.defaultQueryScheme
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.userScheme) }
  }
};
