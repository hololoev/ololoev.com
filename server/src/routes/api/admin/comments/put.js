
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const basicSchemes = require('../../../../libs/schemes/basic');
const commonSchemes = require('../../../../libs/schemes/common');
const buildResult = require('../../../../libs/helpers').buildResult;

async function response(request) {
  const comments = request.getModel(request.server.config.db.database, 'comments');

  let curComment = await comments.findOne({ where: { id: request.params.id } });
  if ( !curComment ) throw Boom.notFound('Комментарий не найден');

  let updComment = Object.assign({}, request.payload);
  updComment.updated_at = new Date();

  await comments.update(updComment, { where: { id: request.params.id } });
  let result = await comments.findOne({ where: { id: request.params.id } });

  return buildResult([ result.dataValues ]);
}

const requestScheme = Joi.object({
  admin_status: Joi.number().integer().min(0).max(1).required().description('Comment admin status').example(1)
});

module.exports = {
  method: 'PUT',
  path: '/api/admin/comments/{id}',
  options: {
    handler: response,
    description: 'Update admin status',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      payload: requestScheme,
      params: Joi.object({
        id: Joi.number().integer().min(0).required().description('Comment id').example(1)
      })
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.commentSchema) }
  }
};
