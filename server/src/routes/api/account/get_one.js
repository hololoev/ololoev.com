
const Boom = require('@hapi/boom');

const basicSchemes = require('../../../libs/schemes/basic');
const commonSchemes = require('../../../libs/schemes/common');
const buildResult = require('../../../libs/helpers').buildResult;

async function response(request) {
  const users = request.getModel(request.server.config.db.database, 'users');

  let query = {
    where: { id: request.auth.artifacts.user.id },
    attributes: users.allowedFields
  };

  let result = await users.findOne(query);

  if ( !result )
    throw Boom.notFound('Пользователь не найден');

  return buildResult([ result.dataValues ]);
}

module.exports = {
  method: 'GET',
  path: '/api/account',
  options: {
    handler: response,
    description: 'Get current user`s account',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.userScheme) }
  }
};
