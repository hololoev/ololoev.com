
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const basicSchemes = require('../../../libs/schemes/basic');
const commonSchemes = require('../../../libs/schemes/common');
const buildResult = require('../../../libs/helpers').buildResult;

async function response(request) {
  const users = request.getModel(request.server.config.db.database, 'users');

  let query = {
    where: { id: request.auth.artifacts.user.id },
    attributes: users.allowedFields
  };

  let curItem = await users.findOne(query);
  if ( !curItem ) throw Boom.notFound('Пользователь не найден');

  let update = Object.assign({ updated_at: new Date() }, request.payload);

  if ( request.payload.email && request.payload.email !== curItem.email )
    update.email_verified = null;

  await users.update(update, { where: { id: request.auth.artifacts.user.id } });

  let result = await users.findOne(query);

  return buildResult([ result.dataValues ]);
}

const requestScheme = Joi.object({
  email: Joi.string().max(255).description('Email').example('ololoev@gmail.com'),
  user_self_description: Joi.string().max(4096).description('User self description').example('dsjnfks dkjkj'),
  avatar_url: Joi.string().max(255).description('Avatar full url').example('https://bebebe.mememe.com/'),
});

module.exports = {
  method: 'PUT',
  path: '/api/account',
  options: {
    handler: response,
    description: 'Update user`s account',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      payload: requestScheme,
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.userScheme) }
  }
};
