
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

// eslint-disable-next-line
const config = require('../../../../../config');
const basicSchemes = require('../../../../libs/schemes/basic');
const commonSchemes = require('../../../../libs/schemes/common');
const buildResult = require('../../../../libs/helpers').buildResult;

async function response(request) {
  const users = request.getModel(request.server.config.db.database, 'users');

  if ( request.payload.new_password !== request.payload.confirmation )
    throw Boom.badRequest('Новый пароль и подтверждение не совподают');

  let query = {
    where: { id: request.auth.artifacts.user.id },
  };

  let curItem = await users.findOne(query);
  if ( !curItem ) throw Boom.notFound('Пользователь не найден');

  if ( !await users.verifyPassword(request.payload.current_password, curItem.password) )
    throw Boom.unauthorized('Неправильный текущий пароль');

  let update = {
    password: request.payload.new_password,
    updated_at: new Date()
  };

  await users.update(update, { where: { id: request.auth.artifacts.user.id } });

  let resQuery = {
    where: { id: request.auth.artifacts.user.id },
    attributes: users.allowedFields
  };
  let result = await users.findOne(resQuery);

  return buildResult([ result.dataValues ]);
}

const requestScheme = Joi.object({
  current_password: Joi.string().min(config.common.min_passwod_length).max(255).description('Email').required().example('Current password'),
  new_password: Joi.string().min(config.common.min_passwod_length).max(255).description('Email').required().example('New password'),
  confirmation: Joi.string().min(config.common.min_passwod_length).max(255).description('Email').required().example('Password confirmation'),
});

module.exports = {
  method: 'PUT',
  path: '/api/account/password',
  options: {
    handler: response,
    description: 'Update user`s password',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      payload: requestScheme,
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.userScheme) }
  }
};
