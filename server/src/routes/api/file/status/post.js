
const Joi = require('@hapi/joi');

const basicSchemes = require('../../../../libs/schemes/basic');
const commonSchemes = require('../../../../libs/schemes/common');
const buildResult = require('../../../../libs/helpers').buildResult;


async function response(request) {
  const files = request.getModel(request.server.config.db.database, 'files');

  let result = await files.findAll({ where: { hash: request.payload } });

  return buildResult(result.map( function(item) {
    return item.get({ plain: true });
  } ));
}

module.exports = {
  method: 'POST',
  path: '/api/file/status',
  options: {
    handler: response,
    description: 'Returns files status by hash',
    tags: [ 'api', 'noTest' ],
    auth: 'token',
    validate: {
      headers: commonSchemes.authHeaders,
      payload: Joi.array().items(Joi.string().max(64).min(10).example('1e4c42db67b2b94b677d04ba4b68e6a5a7e33f34')).max(100)
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.fileSchema) }
  }
};
