
const Joi = require('@hapi/joi');
const crypto = require('crypto');
const Boom = require('@hapi/boom');

const filesHelper = require('../../../libs/filesHelper');
const basicSchemes = require('../../../libs/schemes/basic');
const commonSchemes = require('../../../libs/schemes/common');
const buildResult = require('../../../libs/helpers').buildResult;

const __dstImageExtension = 'jpeg';
const __allowedTypes = {
  'image/jpeg': true,
  'image/jpg': true,
  'image/png': true,
  'image/gif': true
};

function sha1native(buf) {
  const hash = crypto.createHash('sha1');
  hash.update(buf);
  return hash.digest('hex');
}

async function response(request) {
  const files = request.getModel(request.server.config.db.database, 'files');

  if ( !__allowedTypes[ request.payload.type.toLowerCase() ] )
    throw Boom.badRequest('Unsupported file format');

  let fileHash = sha1native(request.payload.data);

  //   let alreadyFile = await files.findOne({ where: { hash: fileHash } });
  //
  //   if( alreadyFile )
  //     return buildResult([ alreadyFile.dataValues ]);

  let newFile = {
    hash: fileHash,
    user_id: request.auth.artifacts.user.id,
    orig_size: request.payload.size,
    status: files.statuses.RECIVED.id,
    content_type: request.payload.type.toLowerCase(),
    orig_name: request.payload.name,
    path: filesHelper.createFilePath(fileHash),
    dst_extension: __dstImageExtension,
    src_ip_address: request.headers[ 'x-real-ip' ] || request.headers[ 'x-forwarded-for' ],
    updated_at: new Date(),
    created_at: new Date()
  };

  let dbFile = await files.create(newFile);

  try {
    await request.server.nats.publish(
      request.server.config.nats.image_q,
      { src: request.payload, dbFile: dbFile.dataValues }
    );
  } catch (err) {
    request.server.logger.serverError(`Cannot publish NATS task: ${err}`);
    await files.destroy({ where: { id: dbFile.id } });
    process.exit(1);
  }

  return buildResult([ dbFile.dataValues ]);
}

module.exports = {
  method: 'POST',
  path: '/api/file',
  options: {
    handler: response,
    description: 'Upload user file',
    tags: [ 'api', 'noTest' ],
    auth: 'token',
    payload: {
      maxBytes: 1024 * 1024 * 16 // 16Mb
    },
    validate: {
      headers: commonSchemes.authHeaders,
      payload: Joi.object({
        type: Joi.string().required().example('image/png'),
        name: Joi.string().required().example('test.png'),
        size: Joi.number().integer().required().example('1245dfg5'),
        data: Joi.string().required().example('base64:data')
      })
    },
    response: { schema: commonSchemes.responseWrapper(basicSchemes.fileSchema) }
  }
};
