
const Joi = require('@hapi/joi');

const commonSchemes = require('../../libs/schemes/common');

async function response(request, h) {
  let authorized = false;
  let user = false;

  try {
    const { artifacts } = await request.server.auth.test('token', request);
    authorized = true;
    user = artifacts.user;
  } catch {}

  const posts = request.getModel(request.server.config.db.database, 'posts');
  const comments = request.getModel(request.server.config.db.database, 'comments');

  let query = {
    where: {
      id: request.params.id
    },
    include: [
      {
        model: posts,
        as: 'post',
        attributes: [ 'id', 'url', 'title' ]
      }
    ]
  };

  let result = await comments.findOne(query);
  result = result.get({ plain: true });

  let page = {
    authorized: authorized,
    user: user,
    page_url: `/comment/${request.params.id}`,
    page_title: result.title,
    page_description: '',
    page_image: '/img/default-200x150.jpg',
    page_create_date: result.created_at,
    page_body: result,
    page_meta: {
      query: request.query || null,
      totalCount: null,
      totalPageCount: null
    }
  };

  return h.view('comment-view', page);
}

module.exports = {
  method: 'GET',
  path: '/comment/{id}',
  options: {
    handler: response,
    description: 'Returns category page',
    tags: [ 'api', 'content', 'noTest' ],
    auth: false,
    validate: {
      headers: commonSchemes.authHeaders,
      params: Joi.object({
        id: Joi.number().integer().required().example(1)
      })
    },
    //response: { schema: commonSchemes.responseWrapper() }
  }
};
