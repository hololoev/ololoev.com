
const commonSchemes = require('../../libs/schemes/common');

async function response(request, h) {
  let authorized = false;
  let user = false;

  try {
    const { artifacts } = await request.server.auth.test('token', request);
    authorized = true;
    user = artifacts.user;

    const accessTokens = request.getModel(request.server.config.db.database, 'access_tokens');
    await accessTokens.destroy({
      where: {
        id: request.auth.artifacts.token.id
      }
    });
  } catch {}

  let page = {
    authorized: authorized,
    user: user,
    page_url: `/logout`,
    page_title: 'Log out...',
    page_description: 'Log out',
    page_image: '/img/default-200x150.jpg',
    page_create_date: new Date(),
    page_body: {},
    page_meta: {
      query: request.query || null,
      totalCount: null,
      totalPageCount: null
    }
  };

  return h.view('logout', page);
}

module.exports = {
  method: 'GET',
  path: '/logout',
  options: {
    handler: response,
    description: 'Unauthorization',
    tags: [ 'api', 'content', 'noTest' ],
    auth: false,
    validate: {
      headers: commonSchemes.authHeaders
    },
    //response: { schema: commonSchemes.responseWrapper() }
  }
};
