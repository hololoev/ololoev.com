
async function response(request, h) {
  let authorized = false;
  let user = false;

  try {
    const { artifacts } = await request.server.auth.test('token', request);
    authorized = true;
    user = artifacts.user;
  } catch {}

  let page = {
    authorized: authorized,
    user: user,
    page_url: '/authorization',
    page_title: 'Авторизация',
    page_description: 'Авторизация',
    page_image: '',
    page_create_date: new Date(),
  };

  return h.view('authorization', page);
}

module.exports = {
  method: 'GET',
  path: '/authorization',
  options: {
    handler: response,
    description: 'Authorization page',
    tags: [ 'api', 'content', 'noTest' ],
    auth: false,
    validate: {
    },
    //response: { schema: commonSchemes.responseWrapper() }
  }
};
