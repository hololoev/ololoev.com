
const statuses = {
  ENABLED: { id: 1, name: 'ENABLED', color: 'green' },
  DISABLED: { id: 0, name: 'DISABLED', color: 'red' }
};

const statusesById = [];
for (let index in statuses)
  statusesById[ statuses[ index ].id ] = statuses[ index ];

module.exports = {
  statuses,
  statusesById
};
