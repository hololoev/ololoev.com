
const statuses = {
  RECIVED: { id: 1, name: 'RECIVED', color: 'grey' },
  PROCESSING: { id: 2, name: 'PROCESSING', color: 'blue' },
  READY: { id: 3, name: 'READY', color: 'green' }
};

const statusesById = [];
for (let index in statuses)
  statusesById[ statuses[ index ].id ] = statuses[ index ];

module.exports = {
  statuses,
  statusesById
};
