const statuses = {
  APPROVED: { id: 1, name: 'APPROVED', color: 'green' },
  PENDING: { id: 0, name: 'PENDING', color: 'red' }
};

const statusesById = [];
for (let index in statuses)
  statusesById[ statuses[ index ].id ] = statuses[ index ];

module.exports = {
  statuses,
  statusesById
};
