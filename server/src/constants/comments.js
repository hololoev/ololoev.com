
const statuses = {
  APPROVED: { id: 1, name: 'APPROVED', color: 'green' },
  BLOCKED: { id: 0, name: 'BLOCKED', color: 'red' }
};

const statusesById = [];
for (let index in statuses)
  statusesById[ statuses[ index ].id ] = statuses[ index ];

module.exports = {
  statuses,
  statusesById
};
