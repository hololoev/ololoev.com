
const statuses = {
  DISABLED: { id: 0, name: 'Отключено', color: 'red', description: 'Сообщество отключено' },
  ENABLED: { id: 1, name: 'Включено', color: 'green', description: 'Сообщество включено' }
};

const entryTypes = {
  CLOSED: { id: 0, name: 'Закрыто', color: 'red', description: 'Вступление закрыто' },
  BY_INVITE: { id: 1, name: 'По инвайтам', color: 'red', description: 'Вступление только по инвайтам' },
  BY_APPROVAL: { id: 2, name: 'По запросу', color: 'red', description: 'Вступление открыто, но нужно подтверждение владелцев сообщества' },
  FREE: { id: 3, name: 'Открыто', color: 'red', description: 'Вступление открыто' },
};

const commentStatuses = {
  DISALLOW: { id: 0, name: 'Отключены', description: 'Анонимные комментарии отключены' },
  ALLOW: { id: 1, name: 'Включены', description: 'Анонимные комментарии включены' },
};

const communityTypes = {
  AUTOCRACY: { id: 1, name: 'Автократия', description: 'Полный произвол владельца сообщества' },
  DEMOCRACY: { id: 2, name: 'Демократия', description: 'Решения принимается большинством голосов, членов сообщества' },
  MERITOCRACY: { id: 3, name: 'Меритократия', description: 'Решения принимаются большинством голосов, самых активных пользователей' }
};

const publicTypes = {
  PUBLIC: { id: 1, name: 'Публичное', description: 'Открыто для просмотра' },
  PRIVATE: { id: 0, name: 'Закрытое', description: 'Просмотр без членства не возможен' },
};

const statusesById = [];
for (let index in statuses)
  statusesById[ statuses[ index ].id ] = statuses[ index ];

const entryTypesById = [];
for (let index in entryTypes)
  entryTypesById[ entryTypes[ index ].id ] = entryTypes[ index ];

const commentStatusesById = [];
for (let index in commentStatuses)
  commentStatusesById[ commentStatuses[ index ].id ] = commentStatuses[ index ];

const communityTypesById = [];
for (let index in communityTypes)
  communityTypesById[ communityTypes[ index ].id ] = communityTypes[ index ];

const publicTypesById = [];
for (let index in publicTypes)
  publicTypesById[ publicTypes[ index ].id ] = publicTypes[ index ];

module.exports = {
  statuses,
  statusesById,
  entryTypes,
  entryTypesById,
  commentStatuses,
  commentStatusesById,
  communityTypes,
  communityTypesById,
  publicTypes,
  publicTypesById
};
