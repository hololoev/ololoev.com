
const Bcrypt = require('bcryptjs');

const constants = require('../constants/users');

const bcryptRounds = 10;

module.exports = (sequelize, DataTypes) => {
  const users = sequelize.define('users', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    avatar_url: {
      type: DataTypes.STRING
    },
    user_self_description: {
      type: DataTypes.TEXT
    },
    email_verified: {
      type: DataTypes.DATE,
      defaultValue: null
    },
    role: {
      type: DataTypes.INTEGER
    },
    status: {
      type: DataTypes.INTEGER
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      set (val) {
        this.setDataValue('password', Bcrypt.hashSync(val, bcryptRounds));
      }
    },
    created_at: {
      type: DataTypes.DATE
    },
    updated_at: {
      type: DataTypes.DATE
    }
  }, {
    timestamps: false,
    indexes: [
      //       {
      //         name: 'blocks_blockchain_id_index',
      //         method: 'BTREE',
      //         fields: [ 'blockchain_id' ]
      //       }
    ]
  });

  users.verifyPassword = function (unencrypted, enctrypted) {
    return Bcrypt.compare(unencrypted, enctrypted);
  };

  users.dummyData = require('./users.json');

  users.allowedFields = [ 'id', 'name', 'email', 'avatar_url', 'user_self_description', 'email_verified', 'role', 'status', 'updated_at', 'created_at' ];

  Object.assign(users, constants);

  return users;
};
