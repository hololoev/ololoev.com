
const constants = require('../constants/files');

module.exports = (sequelize, DataTypes) => {
  const files = sequelize.define('files', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    hash: {
      type: DataTypes.STRING,
      allowNull: false
    },
    path: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValues: null
    },
    dst_extension: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValues: null
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValues: null
    },
    orig_size: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    content_type: {
      type: DataTypes.STRING,
      allowNull: false
    },
    orig_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    src_ip_address: {
      type: DataTypes.STRING
    },
    created_at: {
      type: DataTypes.DATE
    },
    updated_at: {
      type: DataTypes.DATE
    }
  }, {
    timestamps: false,
    indexes: [
      {
        name: 'files_hash',
        method: 'HASH',
        fields: [ 'hash' ]
      },
      {
        name: 'files_path',
        method: 'HASH',
        fields: [ 'path' ]
      },
      {
        name: 'files_user_id',
        method: 'BTREE',
        fields: [ 'user_id' ]
      },
      {
        name: 'files_content_type',
        method: 'BTREE',
        fields: [ 'content_type' ]
      }
    ]
  });

  Object.assign(files, constants);

  files.dummyData = require('./files.json');

  return files;
};
