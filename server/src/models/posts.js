
const constants = require('../constants/posts');

module.exports = (sequelize, DataTypes) => {
  const posts = sequelize.define('posts', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    author_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    community_id: {
      type: DataTypes.INTEGER
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: constants.statuses.DISABLED.id
    },
    admin_status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: constants.statuses.ENABLED.id
    },
    admin_comment: {
      type: DataTypes.TEXT
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    image_url: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    excerpt: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    body: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    src_body: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    published_at: {
      type: DataTypes.DATE
    },
    created_at: {
      type: DataTypes.DATE
    },
    updated_at: {
      type: DataTypes.DATE
    }
  }, {
    timestamps: false,
    indexes: [
      //       {
      //         name: 'blocks_blockchain_id_index',
      //         method: 'BTREE',
      //         fields: [ 'blockchain_id' ]
      //       }
    ]
  });

  posts.associate = function(models) {
    posts.belongsTo(models.users, { as: 'author', foreignKey: 'author_id', targetKey: 'id' });
    posts.belongsTo(models.communities, { as: 'community', foreignKey: 'community_id', targetKey: 'id' });
    posts.hasMany(models.category_relations, { as: 'category_relations', foreignKey: 'post_id', targetKey: 'id' });
  };

  posts.dummyData = require('./posts.json');

  Object.assign(posts, constants);

  return posts;
};
