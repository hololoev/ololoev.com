
const constants = require('../constants/communities');

module.exports = (sequelize, DataTypes) => {
  const communities = sequelize.define('communities', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    type: {
      type: DataTypes.INTEGER, // Типы сообществ: 1- автократия, 2- демократия, 3- Меритократия
      allowNull: false,
      defaultValue: constants.communityTypes.AUTOCRACY.id
    },
    public_type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: constants.publicTypes.PUBLIC.id
    },//ALTER TABLE communities ADD COLUMN public_type int not null after type;
    author_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: constants.statuses.ENABLED.id
    },
    admin_status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: constants.statuses.ENABLED.id
    },
    admin_comment: {
      type: DataTypes.TEXT
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    image_url: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    excerpt: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    body: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    src_body: {
      type: DataTypes.TEXT,
      allowNull: false,
    },

    // OPTIONS
    anon_comments_allow: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: constants.commentStatuses.ALLOW.id
    },
    anon_comments_timeout: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1000 * 60 * 20 // 20 minutes per ip address
    },
    entry: { // Вступление в сообщество
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: constants.entryTypes.FREE.id
    },//--
    non_community_comment_allow: { // Комментарии от зарегистрированных юзеров, но которые не в комьюнити
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: constants.commentStatuses.ALLOW.id
    },
    non_community_comment_timeout: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1000 * 60 * 10 // 10 minutes
    },
    community_comment_timeout: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1000 * 60 * 5 // 5 minutes
    },
    // OPTIONS

    created_at: {
      type: DataTypes.DATE
    },
    updated_at: {
      type: DataTypes.DATE
    }
  }, {
    timestamps: false,
    indexes: [
      {
        name: 'communities_url',
        method: 'BTREE',
        fields: [ 'url' ]
      }
    ]
  });

  communities.associate = function(models) {
    communities.belongsTo(models.users, { as: 'author', foreignKey: 'author_id', targetKey: 'id' });
  };

  communities.dummyData = require('./communities.json');

  Object.assign(communities, constants);

  return communities;
};
