
const Crypto = require('crypto');

module.exports = (sequelize, DataTypes) => {
  const accessToken = sequelize.define('access_tokens', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    token: {
      type: DataTypes.STRING,
      allowNull: false
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    expires_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    created_at: {
      type: DataTypes.DATE
    },
    updated_at: {
      type: DataTypes.DATE
    }
  }, {
    timestamps: false,
    indexes: [
      //       {
      //         name: 'blocks_blockchain_id_index',
      //         method: 'BTREE',
      //         fields: [ 'blockchain_id' ]
      //       }
    ]
  });

  accessToken.generateAccessTokenString = () => {
    // eslint-disable-next-line no-undef
    return Crypto.createHmac('md5', Crypto.randomBytes(512).toString()).update([].slice.call(arguments).join(':')).digest('hex');
  };

  accessToken.createAccessToken = (user) => {
    const options = {
      user_id: user.get('id'),
      expires_at: (new Date(new Date().valueOf() + (30 * 24 * 60 * 60 * 1000))),
      token: accessToken.generateAccessTokenString(user.get('id'), user.get('name'), new Date().valueOf()),
      updated_at: new Date(),
      created_at: new Date()
    };
    return accessToken.create(options);
  };

  accessToken.associate = function(models) {
    accessToken.belongsTo(models.users, { as: 'user', foreignKey: 'user_id', targetKey: 'id' });
  };

  accessToken.dummyData = require('./accessToken.json');

  return accessToken;
};
