
const constants = require('../constants/community_relations');

module.exports = (sequelize, DataTypes) => {
  const community_relations = sequelize.define('community_relations', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    community_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: constants.statuses.PENDING.id
    },
    created_at: {
      type: DataTypes.DATE
    },
    updated_at: {
      type: DataTypes.DATE
    }
  }, {
    timestamps: false,
    indexes: [
      {
        name: 'user_id_index',
        method: 'BTREE',
        fields: [ 'user_id' ]
      },
      {
        name: 'community_id_index',
        method: 'BTREE',
        fields: [ 'community_id' ]
      }
    ]
  });

  community_relations.associate = function(models) {
    community_relations.belongsTo(models.communities, { as: 'community', foreignKey: 'community_id', targetKey: 'id' });
  };

  community_relations.dummyData = require('./community_relations.json');

  Object.assign(community_relations, constants);

  return community_relations;
};
