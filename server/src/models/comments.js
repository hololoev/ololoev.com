
const constants = require('../constants/comments');

module.exports = (sequelize, DataTypes) => {
  const comments = sequelize.define('comments', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    author_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    author_name: {
      type: DataTypes.STRING
    },
    post_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    src_ip_address: {
      type: DataTypes.STRING
    },
    flag_offtopic: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    flag_hidden_count: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    admin_status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: constants.statuses.APPROVED.id
    },
    body: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    src_body: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    created_at: {
      type: DataTypes.DATE
    },
    updated_at: {
      type: DataTypes.DATE
    }
  }, {
    timestamps: false,
    indexes: [
      {
        name: 'created_at',
        method: 'BTREE',
        fields: [ 'created_at' ]
      },
      {
        name: 'updated_at',
        method: 'BTREE',
        fields: [ 'updated_at' ]
      }
    ]
  });

  comments.associate = function(models) {
    comments.belongsTo(models.posts, { as: 'post', foreignKey: 'post_id', targetKey: 'id' });
  };

  comments.dummyData = require('./comments.json');

  Object.assign(comments, constants);

  return comments;
};
