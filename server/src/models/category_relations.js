
const constants = require('../constants/category_relations');

module.exports = (sequelize, DataTypes) => {
  const category_relations = sequelize.define('category_relations', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    post_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    category_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    created_at: {
      type: DataTypes.DATE
    },
    updated_at: {
      type: DataTypes.DATE
    }
  }, {
    timestamps: false,
    indexes: [
      //       {
      //         name: 'blocks_blockchain_id_index',
      //         method: 'BTREE',
      //         fields: [ 'blockchain_id' ]
      //       }
    ]
  });

  category_relations.associate = function(models) {
    category_relations.belongsTo(models.categories, { as: 'category', foreignKey: 'category_id', targetKey: 'id' });
  };

  category_relations.dummyData = require('./category_relations.json');

  Object.assign(category_relations, constants);

  return category_relations;
};
