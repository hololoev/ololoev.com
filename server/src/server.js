'use strict';

const Hapi = require('@hapi/hapi');
const Boom = require('@hapi/boom');
const Inert = require('@hapi/inert');
const filepaths = require('filepaths');
const Sequelize = require('sequelize');
const Vision = require('@hapi/vision');
const HapiSwagger = require('hapi-swagger');
const AuthCookie = require('@hapi/cookie');
const AuthBearer = require('hapi-auth-bearer-token');
const hapiBoomDecorators = require('hapi-boom-decorators');
const bearerValidation = require('./libs/bearerValidation');

const Nats = require('./libs/natslib');
const logger = require('./libs/logger');
const buildResult = require('./libs/helpers').buildResult;

//eslint-disable-next-line
const config = require('../config');
const Package = require('../package');

const swaggerOptions = {
  jsonPath: '/api/documentation.json',
  documentationPath: '/api/documentation',
  info: {
    title: Package.description,
    version: Package.version
  },
};

async function createServer() {

  const nats = new Nats(config.nats.host, config.nats.port);

  try {
    await nats.connect();
  } catch (err) {
    logger.serverError(`Cannot connect to NATS server: ${err}`);
    // eslint-disable-next-line
    process.exit(1);
  }

  const server = await new Hapi.Server(config.server);

  await server.register([
    AuthBearer,
    AuthCookie,
    hapiBoomDecorators,
    Inert,
    Vision,
    {
      plugin: HapiSwagger,
      options: swaggerOptions
    },
    {
      plugin: require('hapi-sequelizejs'),
      options: [
        {
          name: config.db.database,
          models: [ __dirname + '/models/*.js '], // Путь к моделькам
          //ignoredModels: [__dirname + '/server/models/**/*.js'], // if need to ignore some models
          sequelize: new Sequelize(config.db),
          sync: true,
          forceSync: false,
        },
      ],
    }
  ]);

  server.views({
    engines: {
      html: require('handlebars')
    },
    relativeTo: __dirname,
    path: 'templates',
    helpersPath: 'helpers',
    partialsPath: 'partials',
    layout: true,
    layoutPath: 'layouts'
  });

  server.auth.strategy('token', 'bearer-access-token', {
    allowQueryToken: false,
    unauthorized: bearerValidation.unauthorized,
    validate: bearerValidation.validate,
    allowCookieToken: true
  });

  // Load routes
  let routes = filepaths.getSync(__dirname + '/routes/');
  for (let route of routes) {
    let routeConfig;
    try {
      routeConfig = require(route);
    } catch (err) {
      logger.serverError({ message: `Cannot load route: ${route}`, status: err.toString() });
      // eslint-disable-next-line
      process.exit(1);
    }

    //-------------

    // eslint-disable-next-line
    routeConfig.options.validate && Object.assign(routeConfig.options.validate, {
      failAction: (request, h, err) => {
        // Let's override request validation error
        // to diffirentate response validation and request validation
        if (err.name === 'ValidationError') {
          let overridedErr = new Error(err.details[ 0 ].message);
          overridedErr.name = 'RequestValidationError';
          throw Boom.badRequest(overridedErr);
        }

        throw err;
      }
    });

    //----------

    try {
      server.route( routeConfig );
    } catch (err) {
      logger.serverError({ message: `Cannot apply route: ${route}`, status: err.toString() });
      // eslint-disable-next-line
      process.exit(1);
    }
  }

  server.ext({
    type: 'onRequest',
    method: async function (request, h) {
      request.server.config = Object.assign({}, config);
      request.server.nats = nats;
      request.server.logger = logger;
      return h.continue;
    }
  });

  server.ext('onPreResponse', function (request, h) {
    const response = request.response;

    // request.route name
    if ( !response.isBoom ) return h.continue;

    // Wrap error to general response JSON format
    const error = response;
    let statusCode = error.output.statusCode;

    let errorStatus = error.output.payload.error;
    let errorMessage = error.output.payload.message;
    let responseObj = buildResult([], 0, 0, 0, { message: errorMessage, status: errorStatus, description: error.toString() });

    logger.routeError(request, responseObj.meta.error, statusCode);

    return h.response(responseObj).code(statusCode);
  });

  server.events.on('response', (request) => {
    logger.route(request);
  });

  // Start server
  try {
    await server.start();
    logger.info(`Server running at: ${server.info.uri}`);
  } catch (err) {
    logger.serverError({ message: `Cannot start server`, status: err });
    // eslint-disable-next-line
    process.exit(1);
  }

  return server;
}

module.exports = createServer;
