function buildResult(data, total=1, count=1, offset=0, error=null) {
  return {
    data,
    meta: { total, count, offset, error }
  };
}

module.exports = { buildResult };
