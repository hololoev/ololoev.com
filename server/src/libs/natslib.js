const NATS = require('nats');

const __connection_timeout = 10000;

class Nats {

  #nc;
  #url;
  #sid = null;
  #json = true;
  #connected = false;

  constructor(host='127.0.0.1', port=4222, login=null, password=null) {
    let url = `${host}:${port}`;

    if ( login && password )
      url = `${login}:${password}@` + url;

    this.#url = 'nats://' + url;
  }

  connect() {
    return new Promise((resolve, reject) => {
      this.#nc = NATS.connect({ url: this.#url, json: this.#json });

      let timeoutTimer = setTimeout(()=>{
        return reject(new Error('Connection request timed out'));
      }, __connection_timeout);

      this.#nc.on('connect', () => { // nc
        clearTimeout(timeoutTimer);
        this.#connected = true;
        return resolve();
      });

      this.#nc.on('error', (err) => {
        // eslint-disable-next-line
        console.error('NUTS ERROR:', err);
        this.#connected = false;
        // eslint-disable-next-line
        process.exit(1);
      });
    });
  }

  publish(qName, message) {
    return new Promise((resolve, reject) => {
      if ( !this.#connected )
        return reject(new Error('Not connected to NATS server'));

      this.#nc.publish(qName, message, () => {
        return resolve();
      });
    });
  }

  subscribe(qName, cb) {
    if ( !this.#connected )
      throw new Error('Not connected to NATS server');

    if (this.#sid )
      throw new Error('Already subscribed');

    this.#sid = this.#nc.subscribe(qName, cb);
  }

  disconnect() {
    this.#connected = false;
    this.#nc.close();
    this.#nc = null;
  }
}

module.exports = Nats;
