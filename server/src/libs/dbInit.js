
const Sequelize = require('sequelize');
const filepaths = require('filepaths');

function initDB(modelsPath, sequelize) {
  for (let modelFile of filepaths.getSync(modelsPath)) {
    const patt = /\.js$/gi;
    if ( patt.exec(modelFile) ) {
      require(modelFile)(sequelize, Sequelize.DataTypes);
    }
  }
}

module.exports = initDB;
