
const Boom = require('@hapi/boom');
const Op = require('sequelize').Op;

async function unauthorized () {
  throw Boom.unauthorized();
}

async function validate (request, token) {
  const accessToken = request.getModel(request.server.config.db.database, 'access_tokens');
  const users = request.getModel(request.server.config.db.database, 'users');

  let dbToken = await accessToken.findOne({
    where: {
      token: token,
      expires_at: {
        [ Op.gte ]: new Date()
      }
    }
  });

  if ( !dbToken )
    return {
      isValid: false,
      credentials: {}
    };

  let curUser = await users.findOne({
    where: {
      id: dbToken.dataValues.user_id,
      status: users.statuses.ENABLED.id
    },
    attributes: [ 'id', 'name', 'email', 'role', 'status', 'updated_at', 'created_at' ]
  });

  if ( !curUser ) {
    await accessToken.destroy({ where: { user_id: dbToken.dataValues.user_id } });
    return {
      isValid: false,
      credentials: {}
    };
  }

  return {
    isValid: true,
    credentials: {
      role: curUser.role
    },
    artifacts: {
      token: dbToken.dataValues,
      user: curUser.dataValues,
    }
  };
}

module.exports = {
  validate: validate,
  unauthorized: unauthorized
};
