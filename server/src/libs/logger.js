
const appName = 'ololoev-backend';

function sql(sql) {
  // eslint-disable-next-line no-console
  console.log(JSON.stringify({
    date: new Date(),
    app: appName,
    type: 'sql',
    sql: sql
  }));
}

function route(request) {
  // eslint-disable-next-line no-console
  console.log(JSON.stringify({
    date: new Date(),
    app: appName,
    type: 'request',
    method: request.route.method,
    url: request._route.path,
    statusCode: request.raw.res.statusCode,
    remoteAddr: request.headers[ 'x-real-ip' ],
    referrer: request.info.referrer,
    query: request.query,
    params: request.params,
    //payload: request.payload
  }));
}

function routeError(request, error, code) {
  // eslint-disable-next-line no-console
  console.log(JSON.stringify({
    date: new Date(),
    app: appName,
    type: 'route-error',
    method: request.route.method,
    url: request._route.path,
    statusCode: code,
    remoteAddr: request.headers[ 'x-real-ip' ],
    referrer: request.info.referrer,
    query: request.query,
    params: request.params,
    //payload: request.payload,
    error: error
  }));
}

function serverError(error) {
  // eslint-disable-next-line no-console
  console.log(JSON.stringify({
    app: appName,
    type: 'server-error',
    error: error
  }));
}

function info(message) {
  // eslint-disable-next-line no-console
  console.log(JSON.stringify({
    date: new Date(),
    app: appName,
    type: 'info',
    message: message
  }));
}

module.exports = { sql, route, serverError, routeError, info };
