
const MarkdownIt = require('markdown-it');
const mdVideo = require('markdown-it-video');
const html5Embed = require('markdown-it-html5-embed');

const namedCodeBlocks = require('markdown-it-named-code-blocks');

const translit = require('./translit');

const __maxUrlLength = 50;
const __maxUrlRaunds = 30;

async function generateUrl(title, model) {
  let srcUrl = translit(title.trim().substring(0, __maxUrlLength).trim());

  for (let raund=0; raund<__maxUrlRaunds; raund++ ) {
    let url = srcUrl;
    if ( raund > 0 ) url += '_' + raund;

    let dbItem = await model.findOne({ where: { url: url } });
    if ( !dbItem ) return url;
  }

  throw new Error('Cannot create url');
}

function replaceHeaders(srcText) {
  /* eslint-disable */
  srcText = srcText.replace(/<h4/gi, '<h5');
  srcText = srcText.replace(/<h3/gi, '<h4');
  srcText = srcText.replace(/<h2/gi, '<h3');
  srcText = srcText.replace(/<h1/gi, '<h2');
  srcText = srcText.replace(/<\/h4/gi, '</h5');
  srcText = srcText.replace(/<\/h3/gi, '</h4');
  srcText = srcText.replace(/<\/h2/gi, '</h3');
  srcText = srcText.replace(/<\/h1/gi, '</h2');
  return srcText;
  /* eslint-enable */
}

function mdConverter(srcText) {
  let escaped = srcText.replace(/<pre>/gi, '```\n');
  escaped = escaped.replace(/<\/pre>/gi, '```\n');
  escaped = escaped.replace(/</gi, '&lt;');
  escaped = escaped.replace(/>/gi, '&gt;');
  escaped = escaped.replace(/<code>/gi, '```\n');
  escaped = escaped.replace(/<\/code>/gi, '```\n');
  escaped = escaped.replace(/&lt;code&gt;/gi, '```\n');
  escaped = escaped.replace(/&lt;\/code&gt;/gi, '```\n');

  const md = new MarkdownIt({
    html: false
  })
    .use(html5Embed, {
      html5embed: {
        useImageSyntax: true,
        useLinkSyntax: true,
        attributes: {
          'audio': 'width="320" controls class="audioplayer"',
          'video': 'width="600" height="300" class="audioplayer" controls'
        }
      }
    })
    .use(mdVideo, {
      youtube: { width: 600, height: 300 },
      vimeo: { width: 600, height: 300 },
      vine: { width: 600, height: 300, embed: 'simple' },
      prezi: { width: 600, height: 300 }
    })
    .use(namedCodeBlocks);

  try {
    var converted = md.render(escaped);
  } catch (err) {
    console.error('Converter ERROR:', err);
    // eslint-disable-next-line
    process.exit(1);
  }
  /* eslint-disable */
  converted = converted.replace(/&amp;/gi, '&');
  converted = converted.replace(/<table>/gi, '<table class="table table-bordered">');
  return converted;
  /* eslint-enable */
}

module.exports = { generateUrl, replaceHeaders, mdConverter };
