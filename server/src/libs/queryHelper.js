function parseQueryParams(queryParams, raw = false) {
  let result = { where: {} };
  if ( typeof(queryParams.__offset) !== 'undefined' )
    result.offset = queryParams.__offset;

  if ( typeof(queryParams.__count) !== 'undefined' && queryParams.__count )
    result.limit = queryParams.__count;

  if ( queryParams.__order ) result.order = [ parseOrder(queryParams.__order) ];

  if ( raw ) result.raw = true;

  return result;
}

function parseOrder(__order) {
  let orderParts = __order.split(' ');

  if ( orderParts[ 0 ].indexOf('__') ) {
    // Need to sort by subtable
    let fieldParts = orderParts[ 0 ].split('__');
    return [ ...fieldParts, orderParts[ 1 ].toUpperCase() ];
  }

  return orderParts;
}

function parseEOSConsole(str) {
  let parts = str.split('&');
  let result = {};

  for (let part of parts) {
    let chanks = part.split('=');
    result[ chanks[ 0 ] ] = chanks[ 1 ];
  }

  return result;
}

module.exports = {
  parseQueryParams: parseQueryParams,
  parseOrder: parseOrder,
  parseEOSConsole: parseEOSConsole
};
