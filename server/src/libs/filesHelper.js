
const __pathParts = 4;
const __pathPartLength = 1;

function createFilePath(hash) {
  let result = '';

  for (let p=0; p<__pathParts-1; p++)
    result += hash.substring(p * __pathPartLength, p * __pathPartLength + __pathPartLength) + '/';

  result += hash.substring((__pathParts - 1) * __pathPartLength, hash.length);
  return result;
}

module.exports = {
  createFilePath
};
