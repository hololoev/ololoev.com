
const replacements = {
  'й': 'i',
  'ц': 'c',
  'у': 'u',
  'к': 'k',
  'е': 'e',
  'н': 'n',
  'г': 'g',
  'ш': 'sh',
  'щ': 'sh',
  'з': 'z',
  'х': 'h',
  'ф': 'f',
  'ы': 'i',
  'в': 'v',
  'а': 'a',
  'п': 'p',
  'р': 'r',
  'о': 'o',
  'л': 'l',
  'д': 'd',
  'ж': 'j',
  'э': 'e',
  'я': 'ya',
  'ч': 'ch',
  'с': 's',
  'м': 'm',
  'и': 'i',
  'т': 't',
  'б': 'b',
  'ю': 'u',
  'ё': 'e',
  ' ': '_'
};

function translit(text) {
  let _text = text.toLowerCase();
  let result = '';

  for (let symb of _text) {
    if ( replacements[ symb ] ) {
      result += replacements[ symb ];
      continue;
    }

    let charCode = symb.charCodeAt(0);

    if ( (charCode >= 97) && (charCode <= 122) ) { // a...z
      result += symb;
      continue;
    }

    if ( (charCode >= 48) && (charCode <= 57) ) { // 0...9
      result += symb;
      continue;
    }
  }
  return result;
}

module.exports = translit;
