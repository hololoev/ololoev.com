const Joi = require('@hapi/joi');

const __defaultPageLimit = 10;
const __defaultPageNum = 1;

const meta = Joi.object({
  total: Joi.number().integer().required().description('The total number of pages for the given query executed.'),
  count: Joi.number().integer().required().description('The total number of pages for the given query executed on the page.'),
  offset: Joi.number().integer().required().description('The offset for the given query executed on the page.'),
  error: Joi.object({
    message: Joi.string(), details: Joi.string(),
  }).allow(null).description('If error happen it will contain error message.'),
});

function responseWrapper(item=null) {
  if ( item )
    return Joi.object({
      data: Joi.array().items(item),
      meta: meta
    });
  return Joi.object({
    data: Joi.array(),
    meta: meta
  });
}

const authHeaders = Joi.object({
  authorization: Joi.string().allow(null, '').description('Bearer auth token').example('Bearer dd2388e22b58939b34f83ae87bac2c2e')
}).options({ allowUnknown: true });

const defaultQueryScheme = Joi.object({
  __order: Joi.string().description('Query order').example('id asc'),
  __count: Joi.number().integer().min(0).default(20).description('Necessary rows count').example(10),
  __offset: Joi.number().integer().min(0).default(0).description('Ofset').example(0),
  term: Joi.string().min(1).max(255).allow('', null).default(null).description('Any query term').example(null)
});

const defaultPageQueryScheme = Joi.object({
  page: Joi.number().integer().default(__defaultPageNum).description('Page number').example(__defaultPageNum),
  limit: Joi.number().integer().default(__defaultPageLimit).description('Post count on the page').example(__defaultPageLimit)
});

module.exports = {
  meta,
  responseWrapper,
  authHeaders,
  defaultQueryScheme,
  defaultPageQueryScheme
};
