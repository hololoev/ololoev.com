
const Joi = require('@hapi/joi');

const tokenScheme = Joi.object({
  id: Joi.number().integer().example(1),
  token: Joi.string().example('4443655c28b42a4349809accb3f5bc71'),
  user_id: Joi.number().integer().example(2),
  expires_at: Joi.date().allow(null).example(new Date()),
  updated_at: Joi.date().allow(null).example(new Date()),
  created_at: Joi.date().allow(null).example(new Date())
});

const userScheme = Joi.object({
  id: Joi.number().integer().example(1),
  name: Joi.string().example('ololoev'),
  email: Joi.string().example('ololoev@ololoev.com'),
  avatar_url: Joi.string().allow(null, '').example('http://ololo.com/img/avatar.jpg'),
  user_self_description: Joi.string().allow(null, '').example('Ololo, ololo, ya voditel NLO'),
  email_verified: Joi.date().allow(null).example(new Date()),
  status: Joi.number().integer().example(1),
  role: Joi.number().integer().example(1),
  updated_at: Joi.date().allow(null).example(new Date()),
  created_at: Joi.date().allow(null).example(new Date())
});

const messageSchema = Joi.object({
  id: Joi.number().integer().example(1),
  recipient_id: Joi.number().integer(),
  sender_id: Joi.number().integer(),
  status: Joi.number().integer().example(0),
  viewDate: Joi.date().example(new Date()),
  message: Joi.string().example('Lorem ipsum'),
  updated_at: Joi.date().allow(null).example(new Date()),
  created_at: Joi.date().allow(null).example(new Date())
});

const postSchema = Joi.object({
  id: Joi.number().integer().example(1),
  author_id: Joi.number().integer().example(1),
  community_id: Joi.number().integer().allow(null).example(1),
  status: Joi.number().integer().example(1),
  admin_status: Joi.number().integer().example(1),
  admin_comment: Joi.string().allow(null, '').example('Huinya'),
  url: Joi.string().example('ololo_123'),
  image_url: Joi.string().allow(null, '').example('https://ololoev.com/img/test.jpg'),
  description: Joi.string().allow(null, '').example('Moi pizdatiy post'),
  title: Joi.string().example('Moi pizdatiy post'),
  excerpt: Joi.string().allow(null, '').example('Moi pizdatiy post'),
  body: Joi.string().example('Moi pizdatiy post'),
  src_body: Joi.string().example('Moi pizdatiy post'),
  published_at: Joi.date().allow(null).example(new Date()),
  updated_at: Joi.date().allow(null).example(new Date()),
  created_at: Joi.date().allow(null).example(new Date())
});

const communitySchema = Joi.object({
  id: Joi.number().integer().example(1),
  type: Joi.number().integer().example(1),
  public_type: Joi.number().integer().example(1),
  author_id: Joi.number().integer().example(1),
  status: Joi.number().integer().example(1),
  admin_status: Joi.number().integer().example(1),
  admin_comment: Joi.string().allow(null, '').example('Huinya'),
  url: Joi.string().example('ololo_123'),
  image_url: Joi.string().allow(null, '').example('https://ololoev.com/img/test.jpg'),
  description: Joi.string().allow(null, '').example('Mo` pizdatoe community'),
  title: Joi.string().example('Moe pizdatoe community'),
  excerpt: Joi.string().allow(null, '').example('Moe pizdatoe community'),
  body: Joi.string().example('Moi pizdatiy post'),
  src_body: Joi.string().example('Moi pizdatiy post'),
  anon_comments_allow: Joi.number().integer().example(1),
  anon_comments_timeout: Joi.number().integer().example(1),
  entry: Joi.number().integer().example(1),
  non_community_comment_allow: Joi.number().integer().example(1),
  non_community_comment_timeout: Joi.number().integer().example(1),
  community_comment_timeout: Joi.number().integer().example(1),
  updated_at: Joi.date().allow(null).example(new Date()),
  created_at: Joi.date().allow(null).example(new Date())
});

const categorySchema = Joi.object({
  id: Joi.number().integer().example(1),
  author_id: Joi.number().integer().example(1),
  status: Joi.number().integer().example(1),
  url: Joi.string().example('ololo_123'),
  image_url: Joi.string().allow(null, '').example('https://ololoev.com/img/test.jpg'),
  description: Joi.string().allow(null, '').example('Moya categoriya'),
  title: Joi.string().example('Moya categoriya'),
  excerpt: Joi.string().allow(null, '').example('Moya categoriya'),
  body: Joi.string().example('Moya categoriya'),
  src_body: Joi.string().example('Moya categoriya'),
  updated_at: Joi.date().allow(null).example(new Date()),
  created_at: Joi.date().allow(null).example(new Date())
});

const categotyRelationsSchema = Joi.object({
  id: Joi.number().integer().example(1),
  post_id: Joi.number().integer().example(1),
  category_id: Joi.number().integer().example(1),
  updated_at: Joi.date().allow(null).example(new Date()),
  created_at: Joi.date().allow(null).example(new Date())
});

const commentSchema = Joi.object({
  id: Joi.number().integer().example(1),
  author_id: Joi.number().integer().allow(null).example(1),
  author_name: Joi.string().allow(null).example('pupkin'),
  post_id: Joi.number().integer().example(1),
  src_ip_address: Joi.string().example('127.0.0.1'),
  flag_offtopic: Joi.number().integer().example(1),
  flag_hidden_count: Joi.number().integer().example(1),
  admin_status: Joi.number().integer().example(1),
  body: Joi.string().example('ololo'),
  src_body: Joi.string().example('ololo'),
  updated_at: Joi.date().allow(null).example(new Date()),
  created_at: Joi.date().allow(null).example(new Date())
});

const fileSchema = Joi.object({
  id: Joi.number().integer().example(1),
  hash: Joi.string().required().example('112233445678890'),
  path: Joi.string().allow(null).example('a/b/a/sdsadf'),
  dst_extension: Joi.string().allow(null).example('.jpeg'),
  user_id: Joi.number().integer().allow(null).integer().example(2),
  orig_size: Joi.number().integer().example(123),
  status: Joi.number().integer().example(123),
  content_type: Joi.string().required().example('image/png'),
  orig_name: Joi.string().required().example('wet_pussy.png'),
  src_ip_address: Joi.string().required().example('127.0.0.1'),
  updated_at: Joi.date().allow(null).example(new Date()),
  created_at: Joi.date().allow(null).example(new Date())
});

module.exports = {
  tokenScheme,
  userScheme,
  messageSchema,
  postSchema,
  communitySchema,
  categorySchema,
  categotyRelationsSchema,
  commentSchema,
  fileSchema
};
