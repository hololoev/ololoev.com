#!/bin/bash

cd /server

FILE="./.bootstrapped"

npm i
npm i pm2 -g

if [ ! -f $FILE ]; then
  echo "Need to bootstrap"
  cp config_example.js config.js
  sh ./bootstrap_db.sh
  touch $FILE
fi

echo "Starting services..."
pm2 start server.js
pm2 log
