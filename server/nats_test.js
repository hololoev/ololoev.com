'use strict'

const NATS = require('nats');

const config = require('./config');
const Nats = require('./src/libs/natslib');


async function main() {
  const nats = new Nats(config.nats.host, config.nats.port);
  await nats.connect();
  
  nats.publish(config.nats.image_q, 'asdf');
}

main();
