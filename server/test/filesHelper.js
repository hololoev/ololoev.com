
const assert = require('assert');

const filesHelper = require('../src/libs/filesHelper');

describe('Check filesHelper.js', function() {
  
  it('createFilePath', function() {
    const srcHash = 'e7cae5de4954fd09601634c088b405692e60236a';
    assert.equal(filesHelper.createFilePath(srcHash), 'e/7/c/ae5de4954fd09601634c088b405692e60236a');
  })
  
});
