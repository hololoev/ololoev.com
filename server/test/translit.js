
const assert = require('assert');

const translit = require('../src/libs/translit');

const translitExamples = {
  'Письмо о заказе Opencart': 'pismo_o_zakaze_opencart',
  'ё1234567890-=+-) (*?:%;№"!Ёъхзщшгнекуцйфывапролджэ.  юбьтимсчяЯЧСМИТЬБЮ,ЭЖДЛОРПАВЫФЙЦУКЕНГШЩЗХЪ}{POIUYTREWQASDFGHJKL:"?><MNBVCXZzxcvbnm,./;lkjhgfdsaqwertyuiop[]': 'e1234567890_ehzshshgnekucifivaproldje__ubtimschyayachsmitbuejdlorpavificukengshshzhpoiuytrewqasdfghjklmnbvcxzzxcvbnmlkjhgfdsaqwertyuiop'
};

describe('Check translit lib', function() {
  
  it('translit', function() {
    
    for(let input in translitExamples)
      assert.equal(translitExamples[ input ], translit(input));
    
  });
  
});
