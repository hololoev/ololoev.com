
const logger = require('./src/libs/logger');

module.exports = {
  server: {
    host: '0.0.0.0',
    port: 8000
  },
  db: {
    username: process.env.MYSQL_USER || process.env.DBUSER,
    password: process.env.MYSQL_PASSWORD || process.env.DBPASS,
    database: process.env.MYSQL_DATABASE || process.env.DBDATABASE,
    host: process.env.MYSQL_HOST || process.env.DBHOST,
    port: process.env.MYSQL_PORT || process.env.DBPORT,
    dialect: 'mysql',
    dialectOptions: {
      multipleStatements: true
    },
    logging: logger.sql,
    storage: './db/localDB.db' // only for sqlite!
  },
  nats: {
    host: process.env.NATS_HOST || 'nats_q',
    port: process.env.NATS_PORT || 4222,
    image_q: 'imagse'
  },
  common: {
    min_passwod_length: 3
  },
  www_pub: '/www_pub', // see docker config!
  usrImageFolder: '/www_pub/usrimg',
  usrImageWebFolder: '/usrimg',
  main_host: 'https://example.com',
  main_name: 'dev1.ololoev.dev',
  main_phrase: 'ololo test'
};
