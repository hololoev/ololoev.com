'use strict'

const NATS = require('nats');
const sharp = require('sharp');
const makeDir = require('make-dir');
const Sequelize = require('sequelize');
const writeFile =require('fs/promises').writeFile;

const config = require('./config');
const Nats = require('./src/libs/natslib');
const dbInit = require('./src/libs/dbInit');
const filesHelper = require('./src/libs/filesHelper');
const filesConstants = require('./src/constants/files');

const __dstImageMaxSize = 2400;
const __previewMazSize = 1280;
const __iconMazSize = 400;
const __dstQuality = 90;

function resizeImage(image, metadata) {
  let newWidth;
  let newHeight;
  
  if( metadata.width > metadata.height ) {
    newWidth = __dstImageMaxSize;
    newHeight = parseInt((__dstImageMaxSize / metadata.width) * metadata.height);
  } else {
    newHeight = __dstImageMaxSize;
    newWidth = parseInt((__dstImageMaxSize / metadata.height) * metadata.width);
  }
  
  return image.resize(newWidth, newHeight);
}

function makeMetaDescription(dbFile, metadata) {
  let result = `Original file:\n`;
  result += `Name: ${dbFile.orig_name}\n`;
  result += `Type: ${dbFile.content_type}\n`;
  result += `Size: ${dbFile.orig_size}\n`;
  result += `Hash: ${dbFile.hash}\n`;
  result += `\n`;
  result += `Uploaded from: ${dbFile.src_ip_address}\n`;
  result += `Uploaded at: ${dbFile.created_at}\n`;
  result += `\n`;
  result += `Target type: ${dbFile.dst_extension}\n`;
  result += `Target max size: ${__dstImageMaxSize}\n`;
  return result;
}

async function eventHandler(data, db) {
  const { src, dbFile } = data;
  console.log(new Date(), 'Recive file ID:', dbFile.id, 'HASH:', dbFile.hash);
  
  let buffer = Buffer.from(src.data.replace(/^data:image\/(png|gif|jpeg|jpg);base64,/,''), 'base64');
  
  delete src.data; // return memory
  
  let image = await sharp(buffer);
  let metadata = await image.metadata();
  let resized = false;
  
  image = await image.resize(__dstImageMaxSize, __dstImageMaxSize, {
    fit: sharp.fit.inside,
    withoutEnlargement: true
  });
  
  image = await image.toFormat(dbFile.dst_extension, { quality: __dstQuality, progressive: true });
  
  let metaDesc = makeMetaDescription(dbFile, metadata);
  
  let path = config.usrImageFolder + '/' + dbFile.path;
  await makeDir(path);
  await image.toFile(`${path}/fullsize.${dbFile.dst_extension}`);
  await writeFile(`${path}/metadata.txt`, metaDesc, { encoding: 'utf8' });
  
  // then resize to preview
  image = await image.resize(__previewMazSize, __previewMazSize, {
    fit: sharp.fit.inside,
    withoutEnlargement: true
  });

  await image.toFile(`${path}/preview.${dbFile.dst_extension}`);
  
  // then resize to icon
  image = await image.resize(__iconMazSize, __iconMazSize, {
    fit: sharp.fit.inside,
    withoutEnlargement: true
  });
  
  await image.toFile(`${path}/icon.${dbFile.dst_extension}`);
  
  image = null;
  await db.models.files.update({
    status: filesConstants.statuses.READY.id,
    updated_at: new Date()
  }, { where: { id: dbFile.id } });
  
  console.log(new Date(), 'Processed ID:', dbFile.id, 'HASH:', dbFile.hash);
}

async function main() {
  
  console.log(new Date(), 'Load models...');
  let db = new Sequelize(config.db);
  dbInit(__dirname + '/src/models', db);
  console.log(new Date(), 'Modesl Ready');
  
  console.log(new Date(), 'Connecting to NATS...');
  const nats = new Nats(config.nats.host, config.nats.port);
  await nats.connect();
  console.log(new Date(), 'NATS Ready');
  
  console.log(new Date(), 'Image processor ready');
  
  nats.subscribe(config.nats.image_q, async function (data) {
    await eventHandler(data, db);
  });
}

main();
