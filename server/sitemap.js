
const fs = require('fs');
const Op = require('sequelize').Op;
const Sequelize = require('sequelize');

const config = require('./config'); 
const dbInit = require('./src/libs/dbInit');

const __indexFile = `<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <sitemap>
    <loc>${config.main_host}/sitemaps/posts.xml</loc>
  </sitemap>
  <sitemap>
    <loc>${config.main_host}/sitemaps/categories.xml</loc>
  </sitemap>
  <sitemap>
    <loc>${config.main_host}/sitemaps/communities.xml</loc>
  </sitemap>
  <sitemap>
    <loc>${config.main_host}/sitemaps/comments.xml</loc>
  </sitemap>
</sitemapindex>`;

function dateFormat(date) {
  let __date = date || new Date();
  
  return __date.toISOString().split('T')[ 0 ];
}

async function generatePostsMap(db) {
  let result = `<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
`;
  
  let items = await db.models.posts.findAll({
    attributes: [ 'url', 'updated_at' ],
    where: {
      community_id: { [ Op.ne ]: null },
      status: db.models.posts.statuses.ENABLED.id
    }
  });
  
  for(let item of items) {
    result += `  <url>
    <loc>${config.main_host}/post/${item.url}</loc>
    <lastmod>${dateFormat(item.updated_at)}</lastmod>
  </url>
`;
  }
  
  result += '</urlset>';
  
  return result;
}

async function generateCategoriesMap(db) {
  let result = `<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
`;
  
  let items = await db.models.categories.findAll({
    attributes: [ 'url', 'updated_at' ]
  });
  
  for(let item of items) {
    result += `  <url>
    <loc>${config.main_host}/category/${item.url}</loc>
    <lastmod>${dateFormat(item.updated_at)}</lastmod>
  </url>
`;
  }
  
  result += '</urlset>';
  
  return result;
}

async function generateCommunitiesMap(db) {
  let result = `<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
`;
  
  let items = await db.models.communities.findAll({
    attributes: [ 'url', 'updated_at' ]
  });
  
  for(let item of items) {
    result += `  <url>
    <loc>${config.main_host}/community/${item.url}</loc>
    <lastmod>${dateFormat(item.updated_at)}</lastmod>
  </url>
`;
  }
  
  result += '</urlset>';
  
  return result;
}

async function generateCommentsMap(db) {
  let result = `<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
`;
  
  let items = await db.models.comments.findAll({
    attributes: [ 'id', 'updated_at' ]
  });
  
  for(let item of items) {
    result += `  <url>
    <loc>${config.main_host}/comment/${item.id}</loc>
    <lastmod>${dateFormat(item.updated_at)}</lastmod>
  </url>
`;
  }
  
  result += '</urlset>';
  
  return result;
}

async function main() {
  console.log(new Date(), 'Load models...');
  let db = new Sequelize(config.db);
  dbInit(__dirname + '/src/models', db);
  console.log(new Date(), 'Modesl Ready');
  
  if( !fs.existsSync(`${config.www_pub}/sitemaps`) ) {
    console.log(new Date(), `${config.www_pub}/sitemaps`, 'not exists, creating...');
    fs.mkdirSync(`${config.www_pub}/sitemaps`, { recursive: true });
  }
  
  console.log(new Date(), 'Saving main sitemap.xml...');
  fs.writeFileSync(`${config.www_pub}/sitemap.xml`, __indexFile, { encoding: 'utf8' });
  
  // Posts
  console.log(new Date(), 'Generating post map...');
  let postsMap = await generatePostsMap(db);
  
  console.log(new Date(), 'Saving post map...');
  fs.writeFileSync(`${config.www_pub}/sitemaps/posts.xml`, postsMap, { encoding: 'utf8' });
  
  // Categories
  console.log(new Date(), 'Generating category map...');
  let categoriesMap = await generateCategoriesMap(db);
  
  console.log(new Date(), 'Saving category map...');
  fs.writeFileSync(`${config.www_pub}/sitemaps/categories.xml`, categoriesMap, { encoding: 'utf8' });
  
  // Communities
  console.log(new Date(), 'Generating community map...');
  let communitiesMap = await generateCommunitiesMap(db);
  
  console.log(new Date(), 'Saving community map...');
  fs.writeFileSync(`${config.www_pub}/sitemaps/communities.xml`, communitiesMap, { encoding: 'utf8' });

  // comments
  console.log(new Date(), 'Generating comment map...');
  let commentMap = await generateCommentsMap(db);
  
  console.log(new Date(), 'Saving comment map...');
  fs.writeFileSync(`${config.www_pub}/sitemaps/comments.xml`, commentMap, { encoding: 'utf8' });

  
  console.log(new Date(), 'Done');
  process.exit(0);
}

main();
