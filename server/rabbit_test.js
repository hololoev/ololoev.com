'use strict'

const config = require('./config');
const Rabbit = require('./src/libs/rabbitlib');


async function main() {
  
  const producer = new Rabbit(
    config.rabbitmq.host,
    config.rabbitmq.port,
    config.rabbitmq.login,
    config.rabbitmq.password,
    config.rabbitmq.queuePrefix,
    config.rabbitmQueues.images
  );
  
  try {
    await producer.init();
  } catch(err) {
    console.error(new Date(), err);
    process.exit();
  }
  
  await producer.publish({ test: 123 });
  console.log('test');
}

main();
