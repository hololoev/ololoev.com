const fs = require('fs');
const Sequelize = require('sequelize');

const config = require('./config'); 
const dbInit = require('./src/libs/dbInit');
const editorHelper = require('./src/libs/editorHelper');

function createCommentMap(srcList, dstObj) {
  let curID = dstObj.id || '0';
  if( !dstObj.children ) dstObj.children = {};
  
  for(let index=0; index<srcList.length; index++) {    
    if( srcList[ index ].comment_status !== '1' ) continue;    
    
    if( srcList[ index ].parent_id === curID ) {      
      dstObj.children[ srcList[ index ].id ] = srcList[ index ];
      dstObj.children[ srcList[ index ].id ].children = {};
      dstObj.children[ srcList[ index ].id ] = createCommentMap(srcList, dstObj.children[ srcList[ index ].id ]);
    }
  }
  
  return dstObj;
}

function createComment(comment, post_id, realParent=null) {
  let src_body = comment.comment_body;
  src_body = src_body.replace(/<pre>/gi, '```\n');
  src_body = src_body.replace(/<\/pre>/gi, '```\n');
  
  src_body = src_body.replace(/</gi, '&lt;');
  src_body = src_body.replace(/>/gi, '&gt;');
  
  let body = editorHelper.replaceHeaders( editorHelper.mdConverter(src_body) );
  
  if( realParent ) {
    src_body = `>>${realParent}\n\n` + src_body;
    body = `>>${realParent}<br/>\n` + body;    
  }
  
  console.log('**** comment', comment);
  
  let result = {
    author_name: comment.user_name,
    post_id: post_id,
    body: body,
    src_body: src_body,
    created_at: new Date(parseInt(comment.comment_date) * 1000),
    updated_at: Date(parseInt(comment.comment_update_date) * 1000)
  };
  
  if( (comment.user_name == 'Илья') || (comment.user_name == 'Ilya') ) {
    result.author_name = 'ololoev';
    result.author_id = 1;
  }
  
  if( comment.user_role != 'guest' ) {
    result.author_name = 'ololoev';
    result.author_id = 1;
  }
  
  return result;
}

async function saveComments(comMAP, db, post_id, realParent=null) {
  for(let index in comMAP) {
    
    let newComment = createComment(comMAP[ index ], post_id, realParent);
    
    console.log('***', newComment);
    
    let dbComment = await db.models.comments.create(newComment);
    await saveComments(comMAP[ index ].children, db, post_id, dbComment.id);
  }
}

async function main() {
  console.log(new Date(), 'Start importing');
  
  if( process.argv.length < 4 ) {
    console.log('Not enough arguments');
    process.exit();
  }
  
  let dst_post_url = process.argv[ 2 ];
  let src_comments_file = process.argv[ 3 ];
  
  console.log('***', dst_post_url, src_comments_file);
  
  console.log(new Date(), 'Load models...');
  let db = new Sequelize(config.db);
  dbInit(__dirname + '/src/models', db);
  console.log(new Date(), 'Modesl Ready');
  
  let srcPost = await db.models.posts.findOne({ where: { url: dst_post_url } });
  await db.models.comments.destroy({ where: { post_id: srcPost.id } });
  
  let commentsList = JSON.parse(fs.readFileSync(src_comments_file, { encoding: 'utf8' }));
  
  let comMAP = createCommentMap(commentsList, {});
  
  await saveComments(comMAP.children, db, srcPost.id);
  
  console.log(new Date(), 'Done');
}

main();
